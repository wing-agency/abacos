<?php
require(dirname(__FILE__).'/../../../config/config.inc.php');
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once dirname(__FILE__).'/../includes/functions.php';
require_once dirname(__FILE__).'/../includes/webservice.php';

// function updateStock ($row){

// 	return $row;
// }

$debug = array();
// if(Configuration::get('abacos_synchronize_stock')){
	$webserviceABACOS = new webserviceABACOS();

	if($products = $webserviceABACOS->productsAvailable()){
		if(is_array($products) && isset($products['products']) && count($products['products'])){

			if(isset($products['products']['DadosProdutos']['CodigoProduto'])){
				$products['products']['DadosProdutos'] = array($products['products']['DadosProdutos']);
			}

			foreach ($products['products']['DadosProdutos'] as &$row) {

				$associate = Db::getInstance()->getRow("SELECT id_product_ps, id_product_attr_ps, codigo_produto, codigo_abacos, protocolo FROM "._DB_PREFIX_."abacos_product WHERE codigo_abacos = '".$row['CodigoProdutoAbacos'] ."'  AND codigo_produto = '".$row['CodigoProduto']."'");
				if(!$associate){
					$associate = array(
						'id_product_ps'=>0,
						'id_product_attr_ps'=>0, 
						'codigo_produto'=> $row['CodigoProduto'], 
						'codigo_abacos'=> $row['CodigoProdutoAbacos'], 
						'protocolo'=> $row['ProtocoloProduto']
					);
					$dataInsert = $associate;
					$dataInsert['data'] = pSQL(safe_json_encode($row));
					Db::getInstance()->insert('abacos_product', $dataInsert);
				}elseif ($associate['protocolo'] != $row['ProtocoloProduto']){
					$associate['protocolo'] =  $row['ProtocoloProduto'];
					Db::getInstance()->update('abacos_product', array(
						'protocolo'=> $row['ProtocoloProduto'],
						'data'=> pSQL(safe_json_encode($row)),
					), "codigo_abacos = '".$row['CodigoProdutoAbacos'] ."'  AND codigo_produto = '".$row['CodigoProduto']."'");
				}
				if($associate['id_product_ps'] > 0){
					$row['sendproductConfirmAtion'] = sendproductConfirmAtion($row['CodigoProduto']);
					$product = new Product((int)$associate['id_product_ps']);
					if($product->id){
						if($row['Acao'] == 'aieExclusao'){
							$product->active = false;
						}else{
							$product->active = true;
						}
						$product->update();
					}
				}
			}
		}
	}else{
		$debug['error'] =  $webserviceABACOS->getError();
	}
	$debug['results'] = $products; 
// }
die(debug($debug));