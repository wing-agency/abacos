<?php
require(dirname(__FILE__).'/../../../config/config.inc.php');
error_reporting(E_ALL);
ini_set('display_errors', '1');
$useSSL = false;

require_once dirname(__FILE__).'/../includes/functions.php';
require_once dirname(__FILE__).'/../includes/webservice.php';
$debug = array();
// if(Configuration::get('abacos_synchronize_stock')){
if(true){

	// $sql = "SELECT id_product_ps, id_product_attr_ps, codigo_produto, codigo_abacos FROM `"._DB_PREFIX_."abacos_product` WHERE id_product_ps > 0 ";
	// if($results = Db::getInstance()->ExecuteS($sql)){
	// 	$webserviceABACOS = new webserviceABACOS();
	// 	foreach ($results as &$row) {
	// 		$stockAbacos = $webserviceABACOS->stockBalanceOnline($row['codigo_produto']);
	// 		$row['stockAbacos'] = $stockAbacos;
	// 		if($stockAbacos && $stockAbacos['Resultado']['Tipo'] == 'tdreSucesso'){
	// 			StockAvailable::setQuantity((int)$row['id_product_ps'], (int)$row['id_product_attr_ps'], (int) $stockAbacos['SaldoDisponivel']);
	// 			Cache::clean('StockAvailable::getQuantityAvailableByProduct_'.(int)$row['id_product_attr_ps'].'*');
	// 		}
	// 	}
	// 	$debug['results'] = $results;
	// }

	$webserviceABACOS = new webserviceABACOS();
	if($results = $webserviceABACOS->stockAvailable()){
		if(is_array($results)){

			if(isset($results['CodigoProduto'])){
				$results = array($results);
			}

			foreach ($results as &$row) {
				$row['associate'] = Db::getInstance()->getRow("SELECT id_product_ps, id_product_attr_ps FROM `"._DB_PREFIX_."abacos_product` WHERE codigo_produto = '".$row['CodigoProduto']."'");

				if($row['associate'] && $row['associate']['id_product_ps'] >0 && 
					StockAvailable::getQuantityAvailableByProduct($row['associate']['id_product_ps'], $row['associate']['id_product_attr_ps']) != $row['SaldoDisponivel']
				){
					StockAvailable::setQuantity($row['associate']['id_product_ps'], $row['associate']['id_product_attr_ps'], (int)$row['SaldoDisponivel']);
					Cache::clean('StockAvailable::getQuantityAvailableByProduct_'.$row['associate']['id_product_ps'].'*');
					$row['updateStock'] = true;
				}
				$row['confirmStockAvailable'] = $webserviceABACOS->confirmStockAvailable($row['ProtocoloEstoque']);
				if($row['confirmStockAvailable']['ConfirmarRecebimentoEstoqueResult']['Tipo'] != 'tdreSucesso' ){
					logMessageAbacos(array(
						'local'=>'Erro ao confirmar recebimento de estoque',
						$row
					));
				};
			}
		}
	}else{
		$debug['error'] =  $webserviceABACOS->getError();
	}
	$debug['results'] = $results; 
}
die(debug($debug));