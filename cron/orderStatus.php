<?php
require(dirname(__FILE__).'/../../../config/config.inc.php');
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once dirname(__FILE__).'/../includes/functions.php';
require_once dirname(__FILE__).'/../includes/webservice.php';
//PedidosDisponiveis

$webserviceABACOS = new webserviceABACOS();
$debug = array();
if($results = $webserviceABACOS->call('StatusPedidoDisponiveis')){
	if($results['StatusPedidoDisponiveisResult']  && isset($results['StatusPedidoDisponiveisResult']['Rows'])){

		if(isset($results['StatusPedidoDisponiveisResult']['Rows']['DadosStatusPedido']['ProtocoloStatusPedido'])){
			$results['StatusPedidoDisponiveisResult']['Rows']['DadosStatusPedido'] = array($results['StatusPedidoDisponiveisResult']['Rows']['DadosStatusPedido']);
		}
		foreach ($results['StatusPedidoDisponiveisResult']['Rows']['DadosStatusPedido'] as $row) {

// Aguardando Pagamento = aguardando confirmação de pagamento no ábacos
// pagamento confirmado = pagamento confirmado no ábacos
// separado para envio = faturado no ábacos
// enviado = despachado no ábacos
// cancelado = cancelado no ábacos
			$order = new Order($row['NumeroPedido']);
			if($order->id){
				if(strpos(strtolower($row['CodigoStatus']), 'cancelado') !== false){
					$history = new OrderHistory();
					$history->id_order = $order->id;
					$use_existings_payment = false;
					if (!$order->hasInvoice()) {
					    $use_existings_payment = true;
					}
					$history->changeIdOrderState(6, $order, $use_existings_payment);
					$history->add();
					$order->current_state = 6;
				}elseif (strpos(strtolower($row['CodigoStatus']), 'enviado') !== false) {
					// $order = new Order($row['NumeroPedido']);
					$history = new OrderHistory();
					$history->id_order = $order->id;
					$use_existings_payment = false;
					if (!$order->hasInvoice()) {
					    $use_existings_payment = true;
					}
					$history->changeIdOrderState(4, $order, $use_existings_payment);
					$history->add();
					$order->current_state = 4;
				}elseif (strpos(strtolower($row['CodigoStatus']), 'entregue') !== false) {
					// $order = new Order($row['NumeroPedido']);
					$history = new OrderHistory();
					$history->id_order = $order->id;
					$use_existings_payment = false;
					if (!$order->hasInvoice()) {
					    $use_existings_payment = true;
					}
					$history->changeIdOrderState(5, $order, $use_existings_payment);
					$history->add();
					$order->current_state = 5;
				}elseif (strpos(strtolower($row['CodigoStatus']), 'separado para envio') !== false) {
					// $order = new Order($row['NumeroPedido']);
					$history = new OrderHistory();
					$history->id_order = $order->id;
					$use_existings_payment = false;
					if (!$order->hasInvoice()) {
					    $use_existings_payment = true;
					}
					$history->changeIdOrderState(3, $order, $use_existings_payment);
					$history->add();
					$order->current_state = 3;
				}elseif (strpos(strtolower($row['CodigoStatus']), 'pagamento confirmado') !== false) {
					// $order = new Order($row['NumeroPedido']);
					if(strpos(strtolower($order->payment), 'mercadopado') === false){
						$history = new OrderHistory();
						$history->id_order = $order->id;
						$use_existings_payment = false;
						if (!$order->hasInvoice()) {
						    $use_existings_payment = true;
						}
						$history->changeIdOrderState(2, $order, $use_existings_payment);
						$history->add();
						$order->current_state = 2;
					}
				}

				if($row['NumeroObjeto']){
					$order->shipping_number = $row['NumeroObjeto'];
					$id_order_carrier = Db::getInstance()->getValue(' SELECT `id_order_carrier` FROM `'._DB_PREFIX_.'order_carrier` WHERE `id_order` = '.(int)$order->id);
					$order_carrier = new OrderCarrier($id_order_carrier);
					$order_carrier->tracking_number = $row['NumeroObjeto'];
					$order_carrier->update();
				}
				$order->update();
			}
			$webserviceABACOS->call('ConfirmarRecebimentoStatusPedido', array('ProtocoloStatusPedido' => $row['ProtocoloStatusPedido']));
		}
	}
}else{
	$debug['error'] =  $webserviceABACOS->getError();
}
$debug['results'] = $results; 
die(debug(array(
	$debug,
)));

