<?php
require(dirname(__FILE__).'/../../../config/config.inc.php');
require(dirname(__FILE__).'/../../../init.php');
error_reporting(E_ALL);
ini_set('display_errors', '1');

$debug = array();

require_once dirname(__FILE__).'/../includes/functions.php';
require_once dirname(__FILE__).'/../includes/webservice.php';

// if(Configuration::get('abacos_synchronize_stock')){
	$webserviceABACOS = new webserviceABACOS();
	if($results = $webserviceABACOS->priceAvailable()){
		if(is_array($results)){
			if(isset($results['CodigoProduto'])){
				$results = array($results);
			}
			foreach ($results as &$row) {
				$row['associate'] = Db::getInstance()->getRow("SELECT id_product_ps, id_product_attr_ps FROM `"._DB_PREFIX_."abacos_product` WHERE codigo_produto = '".$row['CodigoProduto']."'");

				if($row['associate'] && $row['associate']['id_product_ps'] > 0 ){
					$product = new Product((int)$row['associate']['id_product_ps']);
					$product->price = $row['PrecoPromocional'];
					$product->unit_price = $row['PrecoTabela'];
					$product->unity = $row['PrecoPromocional'];

					$product->update();

					// $row['updatePrice'] = Db::getInstance()->update("product",array('price'=>$row['PrecoTabela']), "id_product = ".$row['associate']['id_product_ps']);
					// Cache::clean('objectmodel_product_'.(int)$row['associate']['id_product_ps'].'_*');
				}

				$row['confirmPriceAvailable'] = $webserviceABACOS->confirmPriceAvailable($row['ProtocoloPreco']);
				if($row['confirmPriceAvailable']['ConfirmarRecebimentoPrecoResult']['Tipo'] != 'tdreSucesso' ){
					logMessageAbacos(array(
						'local'=>'Erro ao confirmar recebimento de preço',
						$row
						));
				};
			}
		}
	}else{
		$debug['error'] =  $webserviceABACOS->getError();
	}
	$debug['results'] = $results; 
// }
die(debug($debug));