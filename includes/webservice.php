<?php
require_once(dirname(__FILE__).'/Nusoap/autoload.php');
require_once(dirname(__FILE__).'/functions.php');

use Aw\Nusoap\NusoapClient;

/**
 * Classe para ajudar na coleta de dados do webservice do ABACOS
 *
 * @author Tcharles Moraes
 */
class webserviceAbacos 
{
	public $prefix = 'abacos_';
	public $module_name = 'abacos';
	private $urlWebservice = null;
	private $connection = null;
	private $error = null;
	
	function __construct()
	{
		$this->setUrlWebservice(Configuration::get($this->prefix.'url_webservice'));

		$this->connect();

	}

	public function setUrlWebservice($url)
	{
		if (!Validate::isAbsoluteUrl($url))
			return false;
		$this->urlWebservice = $url;
		return true;
	}


	protected function connect()
	{
		$this->connection = new NusoapClient($this->urlWebservice,true);
		$this->connection->persistentConnection = true;
		$this->connection->loadWSDL();
		if($this->connection->getError()){
			$this->setError($this->connection->getError());
			return false;
		}
		return true;
	}


	private function setError($error)
	{
		$this->error = utf8_encode($error);
		return true; 
	}

	public function getError()
	{
		return $this->error;
	}


	public function ordersAvailable(){

		$identificationKey = Configuration::get($this->prefix.'key');
		
		$params = array(
		    'ChaveIdentificacao'=> $identificationKey,
		);
		$return = array();
		// PedidosDisponiveis(ChavedeIdentificação: string( 36 );
		$orders =$this->connection->call('PedidosDisponiveis', $params);
		if($this->connection->getError()){
			$this->setError($this->connection->getError());
			return false;
		}
		if ( $orders['PedidosDisponiveisResult']['ResultadoOperacao']['Tipo'] != 'tdreSucesso' ) {
			$this->setError($orders['PedidosDisponiveisResult']['ResultadoOperacao']['Descricao']);
			return false;
		}

		$return['orders'] = isset($orders['PedidosDisponiveisResult']['Rows'])?$orders['PedidosDisponiveisResult']['Rows']:array();
		return $return;
	}

	public function call($name, $params = array()){
		$params = array_merge(array('ChaveIdentificacao'=>Configuration::get($this->prefix.'key')),$params);

		return $this->connection->call($name, $params);
	}



	public function confirmProduct($protocolo){

		$identificationKey = Configuration::get($this->prefix.'key');
		
		$params = array(
		    'ChaveIdentificacao'=> $identificationKey,
		    'ProtocoloProduto'=> $protocolo,
		);

		$result = $this->connection->call('ConfirmarRecebimentoProduto', $params);
		return $result;
	}

	public function productsAvailable(){

		$identificationKey = Configuration::get($this->prefix.'key');
		
		$params = array(
		    'ChaveIdentificacao'=> $identificationKey,
		);
		$return = array();
		// ProdutosDisponiveis(ChavedeIdentificação: string( 36 );
		$products = $this->connection->call('ProdutosDisponiveis', $params);
		
		// echo '<h2>Request Order</h2>';
		// echo '<pre>' . htmlspecialchars($this->connection -> request, ENT_QUOTES) . '</pre>';
		// echo '<h2>Response Order</h2>';
		// echo '<pre>' . htmlspecialchars($this->connection -> response, ENT_QUOTES) . '</pre>';

		if($this->connection->getError()){
			$this->setError($this->connection->getError());
			$this->setError($products);
			return false;
		}

		// ProdutosDisponiveisResult
		if ( $products['ProdutosDisponiveisResult']['ResultadoOperacao']['Tipo'] != 'tdreSucesso' ) {
			$this->setError($products['ProdutosDisponiveisResult']['ResultadoOperacao']['Descricao']);
			
			// return false;
		}
		$return['products'] = isset($products['ProdutosDisponiveisResult']['Rows'])?$products['ProdutosDisponiveisResult']['Rows']:array();
		return $return;
	}
	
	public function productExists( $productID ){

		$identificationKey = Configuration::get($this->prefix.'key');
		
		$params = array(
		    'ChaveIdentificacao'=> $identificationKey,
			'ListaDeNumerosDeProdutosRows' => array (
				'ListaDeNumerosDeProdutos' => array (
					'CodigoProduto' => $productID,
				),
			),
		);
		$return = array();
		// ProdutoExiste
		$product = $this->connection->call('ProdutoExiste', $params);
		if($this->connection->getError()){
			$this->setError($this->connection->getError());
			return false;
		}

		if ( $product['ProdutoExisteResult']['ResultadoOperacao']['Tipo'] != 'tdreSucesso' &&  $product['ProdutoExisteResult']['ResultadoOperacao']['Tipo'] != 'tdreIndefinido') {
			$this->setError($product['ProdutoExisteResult']['ResultadoOperacao']['Descricao']);
			return false;
		}
		$return = isset($products['ProdutoExisteResult']['Rows']['DadosProdutosExistentes']['Existente'])?$products['ProdutoExisteResult']['Rows']['DadosProdutosExistentes']:false;
		return $return;
	}

	public function stockBalanceOnline($productCode) {
		//EstoqueOnLineSaldo

		$identificationKey = Configuration::get($this->prefix.'key');
		
		$params = array(
		    'ChaveIdentificacao'=> $identificationKey,
			'CodigosProduto' => $productCode,
		);
		$return = array();
		$product = $this->connection->call('EstoqueOnLineSaldo', $params);

		// die(debug($product));

		if($this->connection->getError()){
			$this->setError($this->connection->getError());
			return false;
		}

		if ( $product['EstoqueOnLineSaldoResult']['ResultadoOperacao']['Tipo'] != 'tdreSucesso' ) {
			$this->setError($product['EstoqueOnLineSaldoResult']['ResultadoOperacao']['Descricao']);
			return $product['EstoqueOnLineSaldoResult'];
		}
		$return = isset($product['EstoqueOnLineSaldoResult']['Rows']['DadosEstoqueResultado'])?$product['EstoqueOnLineSaldoResult']['Rows']['DadosEstoqueResultado']:false;
		return $return;
	}

	public function stockAvailable() {
		//EstoquesDisponiveis

		$identificationKey = Configuration::get($this->prefix.'key');
		
		$params = array(
		    'ChaveIdentificacao'=> $identificationKey,
		);
		$return = array();
		$stock = $this->connection->call('EstoquesDisponiveis', $params);

		// die(debug($stock));

		if($this->connection->getError()){
			$this->setError($this->connection->getError());
			return false;
		}
		if($stock['EstoquesDisponiveisResult']['ResultadoOperacao']['Tipo'] != 'tdreSucesso' && $stock['EstoquesDisponiveisResult']['ResultadoOperacao']['Tipo'] != 'tdreSucessoSemDados'){
			$this->setError($stock['EstoquesDisponiveisResult']['ResultadoOperacao']['Descricao']);
			return false;
		}elseif ($stock['EstoquesDisponiveisResult']['ResultadoOperacao']['Tipo'] == 'tdreSucessoSemDados') {
			return utf8_encode($stock['EstoquesDisponiveisResult']['ResultadoOperacao']['Descricao']);
		}

		return $stock['EstoquesDisponiveisResult']['Rows']['DadosEstoque'];
		// return $stock;
	}


	public function confirmStockAvailable($protocolo){

		$identificationKey = Configuration::get($this->prefix.'key');
		
		$params = array(
		    'ChaveIdentificacao'=> $identificationKey,
		    'ProtocoloEstoque'=> $protocolo,
		);

		$result = $this->connection->call('ConfirmarRecebimentoEstoque', $params);
		return $result;
	}


	public function priceAvailable() {
		//EstoquesDisponiveis

		$identificationKey = Configuration::get($this->prefix.'key');
		
		$params = array(
		    'ChaveIdentificacao'=> $identificationKey,
		);
		$return = array();
		$stock = $this->connection->call('PrecosDisponiveis', $params);

		// die(debug($stock));

		if($this->connection->getError()){
			$this->setError($this->connection->getError());
			return false;
		}
		if($stock['PrecosDisponiveisResult']['ResultadoOperacao']['Tipo'] != 'tdreSucesso' && $stock['PrecosDisponiveisResult']['ResultadoOperacao']['Tipo'] != 'tdreSucessoSemDados'){
			$this->setError($stock['PrecosDisponiveisResult']['ResultadoOperacao']['Descricao']);
			return false;
		}elseif ($stock['PrecosDisponiveisResult']['ResultadoOperacao']['Tipo'] == 'tdreSucessoSemDados') {
			return utf8_encode($stock['PrecosDisponiveisResult']['ResultadoOperacao']['Descricao']);
		}

		return $stock['PrecosDisponiveisResult']['Rows']['DadosPreco'];
	}


	public function confirmPriceAvailable($protocolo){

		$identificationKey = Configuration::get($this->prefix.'key');
		
		$params = array(
		    'ChaveIdentificacao'=> $identificationKey,
		    'ProtocoloPreco'=> $protocolo,
		);

		$result = $this->connection->call('ConfirmarRecebimentoPreco', $params);
		return $result;
	}

	public function priceOnline($productCode) {
		//PrecoOnline

		$identificationKey = Configuration::get($this->prefix.'key');
		
		$params = array(
		    'ChaveIdentificacao'=> $identificationKey,
		    'ListaDeCodigosProdutos' => array(
				'string' => $productCode,
			),
		);
		$return = array();
		$price = $this->connection->call('PrecoOnline', $params);
		if($this->connection->getError()){
			$this->setError($this->connection->getError());
			return false;
		}

		if ( $price['PrecoOnLineResult']['ResultadoOperacao']['Tipo'] != 'tdreSucesso' ) {
			$this->setError($price['PrecoOnLineResult']['ResultadoOperacao']['Descricao']);
			return false;
		}
		$return = isset($price['PrecoOnLineResult']['DadosEstoqueResultado']['Rows']['DadosPrecoResultado']['PrecoTabela'])?$price['EstoqueOnLineSaldoResult']['PrecoOnLineResult']['Rows']['DadosPrecoResultado']['PrecoTabela']:false;
		return $return;
	}

	public function groupsProductsAvailable()
	{

		$identificationKey = Configuration::get($this->prefix.'key');

		// GrupoProdutosDisponiveis(ChavedeIdentificacao: string);
		$return = array();
		$groups = $this->connection->call('GrupoProdutosDisponiveis',array('ChaveIdentificacao'=> $identificationKey));
		if($this->connection->getError()){
			$this->setError($this->connection->getError());
			return false;
		}
		
		if ( $groups['GrupoProdutosDisponiveisResult']['ResultadoOperacao']['Tipo'] != 'tdreSucesso' ) {
			$this->setError($groups['GrupoProdutosDisponiveisResult']['ResultadoOperacao']['Descricao']);
			return false;
		}
		$return['groups'] = $groups['Rows']['DadosGruposProdutos'];
		return $return;
	}

	public function brandsAvailable()
	{

		$identificationKey = Configuration::get($this->prefix.'key');

		// MarcasDisponiveis(iCodEmp: Integer);
		$return = array();
		$brands = $this->connection->call('MarcasDisponiveis',array('ChaveIdentificacao'=> $identificationKey));
		if($this->connection->getError()){
			$this->setError($this->connection->getError());
			return false;
		}
		
		if ( $brands['MarcasDisponiveisResult']['ResultadoOperacao']['Tipo'] != 'tdreSucesso' ) {
			$this->setError($brands['MarcasDisponiveisResult']['ResultadoOperacao']['Descricao']);
			return false;
		}
		$return['brands'] = $brands['MarcasDisponiveisResult']['Rows']['DadosMarcasProdutos'];
		return $return;
	}

	public function registerCustomer($data){
		// Parâmetros de entrada:
		// CadastrarCliente
			// ChavedeIdentificacao( String:36 ): Identificação única da interface. *CAMPO OBRIGATÓRIO*
			// ListaDeClientes ( Array )
				// DadosClientes ( Array )
					// VersaoWebService( String ): Versão do webservice
					// Email( String:100 ): E-mail. *CAMPO OBRIGATÓRIO*
					// CPFouCNPJ( String:14 ): CPF ou CNPJ. *CAMPO OBRIGATÓRIO*
					// Codigo( String 50 ): Código do cliente/fornecedor. *CAMPO OBRIGATÓRIO*
					// TipoPessoa( Enum( tpeInfinido: Indefinido, tpeFisica ou tpeJuridica )): Pessoa Fisica ou Juridica. *CAMPO OBRIGATÓRIO*
					// Documento( String:14 ): Número de documento
					// InscricaoEstadual ( String:14 ): Inscrição estadual
					// InscricaoMunicipal ( String:14 ): Inscrição municipal
					// Classificacao ( String:50 ): Classificação do cliente/fornecedor, Ex: consumidor
					// Nome( String:100 ): Nome do cliente (Razão social para pessoa jurídica). *CAMPO OBRIGATÓRIO*
					// NomeReduzido( String:20 ): Nome reduzido do cliente (Nome fantasia para pessoa jurídica)
					// Sexo( Enum( tseIndefinido, tseFeminino, tseMasculino ou tseEmpresa )). *CAMPO OBRIGATÓRIO*
					// DataNascimento( String:21 ): Data de nascimento
					// Profissao( String:50 ): Profissão
					// Site( String:100 ): Site
					// Telefone( String:15 ): Telefone
					// Fax( String:15 ): Fax
					// Celular( String:15 ): Celular
					// DataCadastro( String:21 ): Data de cadastro
					// Endereco( Array ) *CAMPO OBRIGATÓRIO*
						// Logradouro( String:80 ): Logradouro *CAMPO OBRIGATÓRIO*
						// NumeroLogradouro( String:50 ): Número do endereço
						// ComplementoEndereco( String:30 ): Complemento do endereço
						// Bairro( String:60 ): Bairro. *CAMPO OBRIGATÓRIO*
						// Municipio( String:60 ): Cidade / Município / Município estrangeiro. *CAMPO OBRIGATÓRIO*
						// Estado( Strin: 60 ): UF / Estado / Estado Estrangeiro. *CAMPO OBRIGATÓRIO*
						// Cep( String:20 ): CEP. *CAMPO OBRIGATÓRIO*
						// TipoLocalEntrega( Enum( tleeDesconhecido, tleeResidencial ou tleeComercial)). *CAMPO OBRIGATÓRIO*
						// ReferenciaEndereco( String:60 ): Referência do endereço
						// Pais( String:100 ): País
						// EndCodigoIBGE( String:8 ): Código IBGE. *CAMPO NÂO DEVE SER ENVIADO*
					// EndCobranca
						// Logradouro( String:80 ): Logradouro *CAMPO OBRIGATÓRIO*
						// NumeroLogradouro( String:50 ): Número do endereço
						// ComplementoEndereco( String:30 ): Complemento do endereço
						// Bairro( String:60 ): Bairro. *CAMPO OBRIGATÓRIO*
						// Municipio( String:60 ): Cidade / Município / Município estrangeiro. *CAMPO OBRIGATÓRIO*
						// Estado( Strin: 60 ): UF / Estado / Estado Estrangeiro. *CAMPO OBRIGATÓRIO*
						// Cep( String:20 ): CEP. *CAMPO OBRIGATÓRIO*
						// TipoLocalEntrega( Enum( tleeDesconhecido, tleeResidencial ou tleeComercial)). *CAMPO OBRIGATÓRIO*
						// ReferenciaEndereco( String:60 ): Referência do endereço
						// Pais( String:100 ): País
						// EndCodigoIBGE( String:8 ): Código IBGE. *CAMPO NÂO DEVE SER ENVIADO*
					// EndEntrega
						// Logradouro( String:80 ): Logradouro *CAMPO OBRIGATÓRIO*
						// NumeroLogradouro( String:50 ): Número do endereço
						// ComplementoEndereco( String:30 ): Complemento do endereço
						// Bairro( String:60 ): Bairro. *CAMPO OBRIGATÓRIO*
						// Municipio( String:60 ): Cidade / Município / Município estrangeiro. *CAMPO OBRIGATÓRIO*
						// Estado( String: 60 ): UF / Estado / Estado Estrangeiro. *CAMPO OBRIGATÓRIO*
						// Cep( String:20 ): CEP. *CAMPO OBRIGATÓRIO*
						// TipoLocalEntrega( Enum( tleeDesconhecido, tleeResidencial ou tleeComercial)). *CAMPO OBRIGATÓRIO*
						// ReferenciaEndereco( String:60 ): Referência do endereço
						// Pais( String:100 ): País
						// EndCodigoIBGE( String:8 ): Código IBGE. *CAMPO NÂO DEVE SER ENVIADO*
					// SubRegiao( Char:50 ): Sub-região do cliente 
					// ClienteEstrangeiro( Char:1 ): Flag para identificar cliente estrangeiro *CAMPO OBRIGATÓRIO*
					// CategoriaFiscal( String:100 ): Categoria Fiscal do cliente
					// RegimeTributario( String ): Regime tributário. *CAMPO NÂO DEVE SER ENVIADO*
					// GrupoCliente( String:50 ): Grupo do cliente
					// ClientesAtributosEstendidos( Array )
						// VersaoWebService( String ): Versão do webservice. *CAMPO NÂO DEVE SER ENVIADO*
					// Rows( Array )
						// DadosClienteAtributosEstendidos( Array )
							// Acao( Enum( aieIndefinido, aieManutencao:Inclusão ou alteração ou aieExclusao ) )
							// NomeAtributo( String:255 ): Nome do atributo
							// ValorAtributo( String:8000 ): Valor do atributo
		
		$dataCustomer = array(
			'ChaveIdentificacao' => Configuration::get($this->prefix.'key'),
			'ListaDeClientes' => array(
				'DadosClientes' => array(
					'Email' => NULL,
					'CPFouCNPJ' => NULL,
					'Codigo' => NULL,
					'TipoPessoa' => NULL,
					'Documento' => NULL,
					'Nome' => NULL,
					'Sexo' => NULL,
					'DataNascimento' => date('d/m/Y'),
					'Telefone' => NULL,
					'Celular' => NULL,
					'DataCadastro' => NULL,
					'Endereco' => array(
						'Logradouro' => NULL,
						'NumeroLogradouro' => NULL,
						'Bairro' => NULL,
						'Municipio' => NULL,
						'Estado' => NULL,
						'Cep' => NULL,
						'TipoLocalEntrega' => NULL,
						'Pais' => NULL, 
					),
					'EndCobranca' => array(
						'Logradouro' => NULL,
						'NumeroLogradouro' => NULL,
						'Bairro' => NULL,
						'Municipio' => NULL,
						'Estado' => NULL,
						'Cep' => NULL,
						'TipoLocalEntrega' => NULL,
						'Pais' => NULL, 
					),
					'EndEntrega' => array(
						'Logradouro' => NULL,
						'NumeroLogradouro' => NULL,
						'Bairro' => NULL,
						'Municipio' => NULL,
						'Estado' => NULL,
						'Cep' => NULL,
						'TipoLocalEntrega' => NULL,
						'Pais' => NULL, 
					),
					'ClienteEstrangeiro' => NULL,
					'GrupoCliente' => NULL,
				)
			)
		);
		$dataCustomer = array_merge($dataCustomer, $data);

		$return = $this->connection->call('CadastrarCliente',$dataCustomer);
		return isset($return['ResultadoOperacao']['Descricao'])?$return['ResultadoOperacao']['Descricao']:$return;
	}

	

	public function insertOrder($dataOrder){
		$dataOrder = (array) $dataOrder;
		
		// Parâmetros de entrada:
		// InserirPedido
			// ChavedeIdentificacao( String:36 ): Identificação única da interface. *CAMPO OBRIGATÓRIO*
			// ListaDePedidos ( Array )
				// DadosPedidos ( Array )
					// NumeroDoPedido( String:50 ): Código do pedido
					// CodigoCliente( String:50 ): Código do cliente. *CAMPO OBRIGATÓRIO*
					// RepresentanteVendas( String:50 ): Representante de vendas
					// CondicaoPagamento( String:50 ): Nome da condição de pagamento *CAMPO OBRIGATÓRIO*
					// ValorPedido( Float ): Valor do pedido *CAMPO OBRIGATÓRIO*
					// ValorFrete( Float ): Valor do frete
					// ValorEncargos( Float ): Valor de encargos
					// ValorDesconto( Float ): Valor do desconto concedido no pedido
					// ValorEmbalagemPresente( Float ): Valor de embalagens para presente
					// ValorReceberEntrega( String:50 ): Valor a receber
					// ValorTrocoEntrega( String:50 ): Valor de troco
 					// DataVenda( Datetime ): Data Venda
					// Transportadora( String:50 ): Nome da transportadora
					// ServicoEntrega( String:50 ): Nome do serviço de entrega
					// Canal( String:50 ): Canal
					// SubCanal( String:50 ): SubCanal
					// EmitirNotaSimbolica( Bool ): Emitir nota fiscal simbólica?
					// ValorCupomDesconto( Float ): Valor do cupom de desconto
					// NumeroCupomDesconto( String:40 ): Número do cupom de desconto
					// PedidoJaPago( Bool ): Pedido já foi pago?
					// DataDoPagamento( Datetime ): Data do pagamento
				// FormasDePagamento( Array ) 
					// DadosPedidosFormaPgto( Array )
						// FormaPagamentoCodigo( Int ): Código da forma de pagamento. O Ábacos não aceita duas formas de pagamento do tipo cupom de desconto.
						// Valor( Float ): Valor
						// CartaoNumero( String:255 ): Numero do cartão
						// CartaoCodigoSegurança( String:255 ): Codigo do cartão de segurança
						// CartaoValidade( String:255 ): Validade do cartão
						// CartaoNomeImpresso( String:50 ): Nome impresso no cartão
						// CartaoQtdeParcelas( Int ): Quantidade de parcelas
						// CartaoCodigoAutorizacao( String:50 ): Codigo autorização cartão
						// BoletoVencimento( Datetime ): Boleto Vencimento
						// BoletoNumeroBancario( String:20 ): Numero Bancário Boleto
						// CartaoCPFouCNPJTitular( Char:14 ): CPF ou CNPJ do titular do cartão de crédito
						// CartaoDataNascimentoTitular( Datetime ): Data de nascimento do titular do cartão
						// DebitoEmContaNumeroBanco( String:10 ): Número do banco para débito em conta corrente
						// DebitoEmContaDVCodigoAgencia( Char:2 ): Digito da agencia para debito em conta
						// DebitoEmContaContaCorrente( String:20 ): Debito em conta
						// DebitoEmContaDVContaCorrente( Char:2 ): Número do dígito da conta para débito em conta corrente
						// PreAutorizadaNaPlataforma( Bool ): Identificar se o pedido teve ou não sua pré-autorização na Plataforma.
						// CartaoTID( String:50 ): Código da transação da empresa intermediária de cobrança.
						// CartaoNSU( String:50 ): Número sequencial único da transação de cartão.
						// CartaoNumeroToken( String:50 ): Número Token do cartão de crédito.
						// CodigoTransacaoGateway( String:100 ): Identificador da transação no gateway. 
				// Itens( Array ) 
					// DadosPedidosItem( Array )
						// CodigoProduto( String:50 )
						// QuantidadeProduto( Float ): Quantidade de produtos
						// PrecoUnitario( Float ): Preço unitário
						// EmbalagemPresente( Bool ): Embalagem para presente?
						// MensagemPresente( Text ): Mensagem presente
						// PrecoUnitarioBruto( Float ): Preço unitário bruto
						// Brinde( Bool ): Brinde?
						// ValorReferencia( Float ): Valor referencia
						// Personalizacao( Array )
							// DadosPedidosItemPersonalizacao( Array )
								// CodigoProdutoPersonalizacao( String:50 ): Código do produto personalização
								// PrecoUnitarioLiquido( Float ): Preço unitário liquido
								// PrecoUnitarioBruto( Float ): Preço unitário bruto
								// TextoPersonalizacao( Text ): Texto associado a personalização do item do pedido
								// DescritorSimples1( String:100 ): Descritor simples do produto
								// DescritorSimples2( String:100 ): Descritor simples do produto
								// DescritorSimples3( String:100 ): Descritor simples do produto
								// DescritorPreDefinido1( String:100 ): Nome do descritor pré definido do produto
								// DescritorPreDefinido2( String:100 ): Nome do descritor pré definido do produto
								// DescritorPreDefinido3( String:100 ): Nome do descritor pré definido do produto
				// OptouNFPaulista( Enum:( tbneNenhum, tbneSim ou tbneNao ) ): O cliente optou por NF Paulista?
				// CodigoCartaoPresente( String:50 ): Código do cartão presente
				// MensagemCartaoPresente( Text ): Mensagem para o cartão
				// AssinaturaCartaoPresente( String:100 ): Assinatura do cartão
				// ValorTotalCartaoPresente( Float ): Valor total do cartão
				// CartaoPresenteBrinde( Bool ): O cartão de presente é um brinde?
				// TempoEntregaTransportadora( Int ): Tempo de entrega da transportadora
				// DataPrazoEntregaInicial( Datetime ): Data inicial do prazo de entrega
				// DataPrazoEntregaFinal( Datetime ): Data final do prazo de entrega
				// TipoEntrega( String:50 ): Tipo de entrega
				// CodigoLocalRetirada( String:50 ) Local retirada
				// SenhaRetirada( String:50 ): Senha retirada
				// IPComprador( String:50 ): IP do usuário quando este efetuou a compra
				// ComercializacaoOutrasSaidas( Int ): Comercialização outras saídas
				// PrazoEntregaPosPagamento( Float ): Prazo de entrega pós pagamento
				// NotaRemessa
					// NumeroNotaFiscal( Int ): Numero da Nota para Remessa
					// SerieNotaFiscal( String:3 ): Serie da Nota para Remessa
					// DataEmissaoNotaFiscal( Datetime ): Data de emissão da Nota para Remessa
				// ValorFretePagar( Float ): Valor do frete a ser pago pela empresa
				// CampoUsoLivre( String:50 ): Campo uso livre
				// CodigoListaPresente( String:50 ): Código lista de presente
				// TelefoneSMS( String:20 ): Numero de telefone para SMS
		


		$paramsOrder = array(
			'ChaveIdentificacao' => Configuration::get($this->prefix.'key'),
			'ListaDePedidos' => array (
				'DadosPedidos' => array (
					'NumeroDoPedido' => NULL,
					'CodigoCliente' => NULL,
					'RepresentanteVendas' => NULL,
					'CondicaoPagamento' => NULL,
					'ValorPedido' => NULL,
					'ValorFrete' => NULL,
					'ValorDesconto' => NULL,
 					'DataVenda' => date('d/m/Y'),
					'Transportadora' => NULL,
					'ServicoEntrega' => NULL,
					'EmitirNotaSimbolica' => 1,
					'ValorCupomDesconto' => NULL,
					'NumeroCupomDesconto' => NULL,
					'PedidoJaPago' => 0,
					'DataDoPagamento' => date('d/m/Y'),
					'Lote' => 0,
				),
				'FormasDePagamento' => array ( 
					'DadosPedidosFormaPgto' => array (
						'FormaPagamentoCodigo' => NULL,
						'Valor' => NULL,
						'CartaoNumero' => NULL,
						'CartaoCodigoSegurança' => NULL,
						'CartaoValidade' => NULL,
						'CartaoNomeImpresso' => NULL,
						'CartaoQtdeParcelas' => NULL,
						'CartaoCodigoAutorizacao' => NULL,
						'BoletoVencimento' => NULL,
						'BoletoNumeroBancario' => NULL,
						'CartaoCPFouCNPJTitular' => NULL,
						'CartaoDataNascimentoTitular' => NULL,
						'DebitoEmContaNumeroBanco' => NULL,
						'DebitoEmContaDVCodigoAgencia' => NULL,
						'DebitoEmContaContaCorrente' => NULL,
						'DebitoEmContaDVContaCorrente' => NULL,
						'PreAutorizadaNaPlataforma' => NULL,
						'CartaoTID' => NULL,
						'CartaoNSU' => NULL,
						'CartaoNumeroToken' => NULL,
						'CodigoTransacaoGateway' => NULL,
					),
				),
				'Itens' => array (
					'DadosPedidosItem' => array (
						'CodigoProduto' => NULL,
						'QuantidadeProduto' => NULL,
						'PrecoUnitario' => NULL,
						'EmbalagemPresente' => NULL,
 						'MensagemPresente' => NULL,
						'PrecoUnitarioBruto' => NULL,
						'Brinde' => NULL,
						'ValorReferencia' => NULL,
					),
				),
				'OptouNFPaulista' => 'tbneNenhum',
				'TempoEntregaTransportadora' => NULL,
				'DataPrazoEntregaInicial' => date('d/m/Y'),
				'DataPrazoEntregaFinal' => date('d/m/Y'),
				'TipoEntrega' => NULL,
				'PrazoEntregaPosPagamento' => NULL,
				'NotaRemessa' => array(
					'NumeroNotaFiscal' => NULL,
					'SerieNotaFiscal' => NULL,
					'DataEmissaoNotaFiscal' => NULL,
				),
				'CartaoPresenteBrinde' => 0,
				'ValorFretePagar' => NULL,
				'TelefoneSMS' => NULL,
			),
		);

		$dataOrder = array_merge($paramsOrder, $dataOrder);

		$orderInsert = ($this->connection->call('InserirPedido',$dataOrder));
		// echo '<h2>Request Order</h2>';
		// echo '<pre>' . htmlspecialchars($this->connection -> request, ENT_QUOTES) . '</pre>';
		// echo '<h2>Response Order</h2>';
		// echo '<pre>' . htmlspecialchars($this->connection -> response, ENT_QUOTES) . '</pre>';
	    // die();
		// die(debug($this->connection->debug_str));
		
		// $idOrderABACOS = (isset($return['InserirPedidoResult']) && isset($return['InserirPedidoResult']['Rows']['DadosPedidosResultado']['NumeroDoPedido'])) ? $return['InserirPedidoResult']['Rows']['DadosPedidosResultado']['NumeroDoPedido'] : $return['InserirPedidoResult']['ResultadoOperacao']['Descricao'];
		// return ($idOrderABACOS) ? $idOrderABACOS : false;
		return $orderInsert;
	}
}