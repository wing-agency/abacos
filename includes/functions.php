<?php

if (!function_exists( 'debug' ) ) {
	function debug($params){
		$content = '&nbsp<pre>';
		$content.= print_r($params,true);
		$content.= '</pre>';
		return $content;
	}
}

if (!function_exists( 'logMessageAbacos' ) ) {
	function logMessageAbacos($message, $level = 1, $file = null){
		$file = ($file != null)? $file : dirname(dirname(__FILE__)).'/logs/log.log';
		return logMessage($message, $level, $file);
	}
}


if (!function_exists( 'logMessage' ) ) {
	/**
	* Write the message in the log file
	*
	* @param string message
	* @param level  0 => 'DEBUG',1 => 'INFO',2 => 'WARNING',3 => 'ERROR',
	*/
	function logMessage($message, $level = 1, $file = null){
		$logger = new FileLogger();
		$file = ($file != null)? $file : dirname(dirname(__FILE__)).'/logs/log.log';
		$logger->setFilename($file);

		if(is_array($message) || is_object($message)){
			if(function_exists('print_r')){
				$message = print_r($message,true);
			}else{
				ob_start();
				var_dump($message);
				$message = ob_get_contents();
				ob_end_clean();
			}
		}

		return $logger->log($message, $level);
	}
}

if (!function_exists( 'xmlToArr' ) ) {
	function xmlToArr ($xml, $forceArray = true){
		return json_decode(json_encode($xml), $forceArray );
	}
}

if (!function_exists( 'sortByParam' ) ) {
	
	function sortByParam(&$array,$param)
	{
		mySortClass::$param = $param;
		usort($array, array('mySortClass', 'cmp_obj'));
		return $array; 
	}

	class mySortClass
	{
		static $param = null;

		static function cmp_obj($a, $b)
		{
			$return = null;
			$a = (array) $a;
			$b = (array) $b;

			return strcasecmp($a[self::$param], $b[self::$param]);
		}
	}
}

if (!function_exists( 'convertPriceBrlToUsd' ) ) {
	function convertPriceBrlToUsd($get_valor) {
		$source = array('.', ',');
		$replace = array('', '.');
		$valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
		return $valor; //retorna o valor formatado para gravar no banco
	}
}

if (!function_exists( 'saveRelationERP' ) ) {
	function saveRelationERP($idERP, $idProduct, $idAttribute = 0){
		$data = array(
			'id_product_ps'=>(int)$idProduct,
			'id_product_attr_ps'=>(int)$idAttribute,
			'id_product_erp'=>$idERP
			);
		$sql = "SELECT id_product_erp FROM "._DB_PREFIX_."erp_captare_product WHERE id_product_erp = '".$idERP."'";
		$select = Db::getInstance()->getValue($sql);
		if($select){
			return Db::getInstance()-> update('erp_captare_product',$data, "id_product_erp = '".$idERP."'");
		}else{
			return Db::getInstance()->insert('erp_captare_product',$data);
		}
	}
}
if (!class_exists('WebservicecaptareException')){
	class WebservicecaptareException extends PrestaShopException
	{
	/**
	 * Log the error on the disk
	 */
	protected function logError()
	{
		$logger = new FileLogger();
		$logger->setFilename(dirname(dirname(__FILE__)).'/logs/'.date('Ymd').'_exception.log');
		$logger->logError($this->getExtendedMessage(false));
		}
	}
}



if (!function_exists( 'safe_json_encode' ) ) {
	function safe_json_encode($value){
		if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
			$encoded = json_encode($value, JSON_PRETTY_PRINT);
		} else {
			$encoded = json_encode($value);
		}
		switch (json_last_error()) {
			case JSON_ERROR_NONE:
				return $encoded;
			case JSON_ERROR_DEPTH:
				return 'Maximum stack depth exceeded'; // or trigger_error() or throw new Exception()
			case JSON_ERROR_STATE_MISMATCH:
				return 'Underflow or the modes mismatch'; // or trigger_error() or throw new Exception()
			case JSON_ERROR_CTRL_CHAR:
				return 'Unexpected control character found';
			case JSON_ERROR_SYNTAX:
				return 'Syntax error, malformed JSON'; // or trigger_error() or throw new Exception()
			case JSON_ERROR_UTF8:
				$clean = utf8ize($value);
				return safe_json_encode($clean);
			default:
				return 'Unknown error'; // or trigger_error() or throw new Exception()
		}
	}

	function utf8ize($mixed) {
		if (is_array($mixed)) {
			foreach ($mixed as $key => $value) {
				$mixed[$key] = utf8ize($value);
			}
		} else if (is_string ($mixed)) {
			return utf8_encode($mixed);
		}
		return $mixed;
	}
}


if (!function_exists( 'sendProductConfirmation' ) ) {

	function sendProductConfirmation($codigoProduto){
		$protocolo = Db::getInstance()->getValue("SELECT protocolo FROM "._DB_PREFIX_."abacos_product WHERE  codigo_produto = '".$codigoProduto."'");

		$webserviceABACOS = new webserviceABACOS();

		$confirmProduct = $webserviceABACOS->confirmProduct($protocolo);

		if($confirmProduct['ConfirmarRecebimentoProdutoResult']['Tipo'] == 'tdreSucesso'){
			return true;
		}
		logMessageAbacos(array(
			'local'=>'Erro ao confirmar integração com o produto',
			'protocolo'=>$protocolo,
			'confirmProduct'=>$confirmProduct,
		));
		return false;

	}

}


if (!function_exists( 'sendOrderToAbacos' ) ) {
	function sendOrderToAbacos($idOrder){
		$order = new Order($idOrder);
		if(!$order->id){
			throw new Exception("O pedido '{$idOrder}' não foi encontrado.", 404);
			return;
		}

		$id_lang = Context::getContext()->language->id;
		$products = $order->getProductsDetail();


		$customer = $order->getCustomer();
		$addressDelivery = new Address($order->id_address_delivery);
		$addressDelivery->state =  Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'state WHERE id_state = '.$addressDelivery->id_state);
		if($order->id_address_delivery != $order->id_address_invoice){
			$addressInvoice = new Address($order->id_address_invoice);
			$addressInvoice->state =  Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'state WHERE id_state = '.$addressInvoice->id_state);
		}else{
			$addressInvoice = $addressDelivery;
		}

		$webserviceABACOS = new webserviceABACOS();
		$idCustomerABACOS = false;

		$dataCustomer = array(
			'ListaDeClientes' => array(
				'DadosClientes' => array(
					'Codigo' => $customer->id,
					'EMail' => $customer->email,
					'CPFouCNPJ' => preg_replace("/[^0-9]/", "", $customer->document),
					'TipoPessoa' => ($customer->doc_type == 2) ? 'tpeFisica' : 'tpeJuridica',
					'Documento' => $customer->rg_ie,
					'Nome' => $customer->firstname.' '.$customer->lastname,
					'Sexo' => ($customer->doc_type == 1) ? 'tseEmpresa' : ($customer->id_gender == 1) ? 'tseMasculino' : 'tseFeminino',
					'DataNascimento' => date('dmY' , strtotime($customer->birthday)),
					'Telefone' => $addressDelivery->phone,
					'Celular' => $addressDelivery->phone_mobile,
					'Endereco' => array(
						'Logradouro' => $addressInvoice->address1,
						'NumeroLogradouro' => $addressInvoice->number,
						'Bairro' => $addressInvoice->address2,
						'Municipio' => $addressInvoice->city,
						'Estado' => $addressInvoice->state,
						'Cep' => preg_replace("/[^0-9]/", "", $addressInvoice->postcode),
						'TipoLocalEntrega' => NULL,
						'Pais' => 'Brasil', 
					),
					'EndEntrega' => array(
						'Logradouro' => $addressDelivery->address1,
						'NumeroLogradouro' => $addressDelivery->number,
						'Bairro' => $addressDelivery->address2,
						'Municipio' => $addressDelivery->city,
						'Estado' => $addressDelivery->state,
						'Cep' => preg_replace("/[^0-9]/", "", $addressDelivery->postcode),
						'TipoLocalEntrega' => NULL,
						'Pais' => 'Brasil', 
					),
					'EndCobranca' => array(
						'Logradouro' => $addressInvoice->address1,
						'NumeroLogradouro' => $addressInvoice->number,
						'Bairro' => $addressInvoice->address2,
						'Municipio' => $addressInvoice->city,
						'Estado' => $addressInvoice->state,
						'Cep' => preg_replace("/[^0-9]/", "", $addressInvoice->postcode),
						'TipoLocalEntrega' => NULL,
						'Pais' => 'Brasil', 
					),
					'ClienteEstrangeiro' => NULL,
					'GrupoCliente' => NULL,
					'Classificacao' => 'CONSUMIDOR',
					'DataCadastro' => date('dmY' , strtotime($customer->date_add)),
				)
			)
		);

		// EndCobranca' => array(
		// 	'Logradouro' => NULL,
		// 	'NumeroLogradouro' => NULL,
		// 	'Bairro' => NULL,
		// 	'Municipio' => NULL,
		// 	'Estado' => NULL,
		// 	'Cep' => NULL,
		// 	'TipoLocalEntrega' => NULL,
		// 	'Pais' => NULL, 
		// ),
		// 'EndEntrega' => array(
		// 	'Logradouro' => NULL,
		// 	'NumeroLogradouro' => NULL,
		// 	'Bairro' => NULL,
		// 	'Municipio' => NULL,
		// 	'Estado' => NULL,
		// 	'Cep' => NULL,
		// 	'TipoLocalEntrega' => NULL,
		// 	'Pais' => NULL, 
		// ),




		$insertCustomer = $webserviceABACOS->registerCustomer($dataCustomer);

		if(!$insertCustomer || !$insertCustomer['CadastrarClienteResult'] || $insertCustomer['CadastrarClienteResult']['ResultadoOperacao'] ['Tipo'] != 'tdreSucesso'){
			$msg = 'Erro ao cadastrar o client "'.$customer->firstname.' '.$customer->lastname.'" no ABACOS, tente novamente mais tarde.';
			$msg .= (!$insertCustomer['CadastrarClienteResult'])?'':'<br/>'.utf8_encode($insertCustomer['CadastrarClienteResult']['ResultadoOperacao']['Descricao']);
			throw new Exception($msg, 404);
			return;
		}

		$productsABACOS = array();

		foreach ($products as $key => $product) {
			$codigoProduto = Db::getInstance()->getValue('SELECT codigo_produto FROM '._DB_PREFIX_.'abacos_product WHERE id_product_ps = "'.$product['product_id'].'" AND  id_product_attr_ps = "'.$product['product_attribute_id'].'"');

			if(!$codigoProduto){
				$sql = "SELECT p.ean13, p.reference, pa.reference AS reference_att, pa.ean13 AS ean13_attr
						FROM `"._DB_PREFIX_."product` AS p 
						LEFT JOIN `"._DB_PREFIX_."product_attribute` AS pa ON (p.id_product = pa.id_product AND pa.id_product_attribute = ".$product['product_attribute_id']." )
						WHERE p.id_product = ".$product['product_id'];

				$infoProduct = Db::getInstance()->getRow($sql);
				if($infoProduct){
					$codigoProduto = ($infoProduct['ean13'] != '') ? $infoProduct['ean13'] : $codigoProduto;
					$codigoProduto = ($infoProduct['ean13_attr'] != '') ? $infoProduct['ean13_attr'] : $codigoProduto;
					$codigoProduto = ($infoProduct['reference'] != '') ? $infoProduct['reference'] : $codigoProduto;
					$codigoProduto = ($infoProduct['reference_att'] != '') ? $infoProduct['reference_att'] : $codigoProduto;
				}
			}


//         <abac:DadosPedidosItem>
//             <abac:CodigoProduto>17228</abac:CodigoProduto>
//             <abac:QuantidadeProduto>1</abac:QuantidadeProduto>
//             <abac:PrecoUnitario>57.81</abac:PrecoUnitario>
//             <abac:EmbalagemPresente>false</abac:EmbalagemPresente>
//             <abac:MensagemPresente/>
//             <abac:PrecoUnitarioBruto>28</abac:PrecoUnitarioBruto>
//             <abac:Brinde>false</abac:Brinde>
//         </abac:DadosPedidosItem>

			$productsABACOS[] = array (
				'CodigoProduto' => $codigoProduto,
				'QuantidadeProduto' => $product['product_quantity'],
				'PrecoUnitario' => number_format($product['price'], 2),
				'EmbalagemPresente' => 0,
				'MensagemPresente' => '',
				'PrecoUnitarioBruto' => number_format($product['wholesale_price'], 2),
				'Brinde' => 0,
			);
		}

// <abac:DadosPedidos>
//     <abac:NumeroDoPedido>1401</abac:NumeroDoPedido>
//     <abac:EMail>integracao@kpl.com.br</abac:EMail>
//     <abac:CPFouCNPJ>05521031000101</abac:CPFouCNPJ>
//     <abac:CodigoCliente>KPL</abac:CodigoCliente>
//     <abac:ValorPedido>57.81</abac:ValorPedido>
//     <abac:ValorFrete>0</abac:ValorFrete>
//     <abac:ValorEncargos>0</abac:ValorEncargos>
//     <abac:ValorDesconto>0</abac:ValorDesconto>
//     <abac:ValorEmbalagemPresente>0</abac:ValorEmbalagemPresente>
//     <abac:DataVenda>14012016</abac:DataVenda>
//     <abac:Transportadora>Integracao</abac:Transportadora>
//     <abac:EmitirNotaSimbolica>false</abac:EmitirNotaSimbolica>
//     <abac:ValorCupomDesconto>0</abac:ValorCupomDesconto>
//     <abac:NumeroCupomDesconto>0</abac:NumeroCupomDesconto>
//     <abac:DestNome>KPL</abac:DestNome>
//     <abac:DestSexo>tseMasculino</abac:DestSexo>
//     <abac:DestEmail>integracao@kpl.com.br</abac:DestEmail>
//     <abac:DestTelefone>21879090</abac:DestTelefone>
//     <abac:DestLogradouro>Alameda Cauaxi</abac:DestLogradouro>
//     <abac:DestNumeroLogradouro>350</abac:DestNumeroLogradouro>
//     <abac:DestComplementoEndereco>5 andar</abac:DestComplementoEndereco>
//     <abac:DestBairro>Alphaville</abac:DestBairro>
//     <abac:DestMunicipio>Barueri</abac:DestMunicipio>
//     <abac:DestEstado>SP</abac:DestEstado>
//     <abac:DestCep>06454020</abac:DestCep>
//     <abac:DestTipoLocalEntrega>tleeComercial</abac:DestTipoLocalEntrega>
//     <abac:DestEstrangeiro>N</abac:DestEstrangeiro>
//     <abac:DestPais>Brasil</abac:DestPais>
//     <abac:DestCPF>05521031000101</abac:DestCPF>
//     <abac:DestTipoPessoa>tpeJuridica</abac:DestTipoPessoa>
//     <abac:Anotacao1>Integracao</abac:Anotacao1>
//     <abac:PedidoJaPago>false</abac:PedidoJaPago>
//     <abac:DataDoPagamento>14012016</abac:DataDoPagamento>
//     <abac:FormasDePagamento>
//         <abac:DadosPedidosFormaPgto>
//             <abac:FormaPagamentoCodigo>Mastercard</abac:FormaPagamentoCodigo>
//             <abac:Valor>57.81</abac:Valor>
//             <abac:CartaoNumero>4012001037141112</abac:CartaoNumero>
//             <abac:CartaoCodigoSeguranca>123</abac:CartaoCodigoSeguranca>
//             <abac:CartaoValidade>122015</abac:CartaoValidade>
//             <abac:CartaoNomeImpresso>KPL Solucoes</abac:CartaoNomeImpresso>
//             <abac:CartaoQtdeParcelas>1</abac:CartaoQtdeParcelas>
//             <abac:CartaoCodigoAutorizacao>4444444444</abac:CartaoCodigoAutorizacao>
//             <abac:CartaoCPFouCNPJTitular>05521031000101</abac:CartaoCPFouCNPJTitular>
//             <abac:CartaoDataNascimentoTitular>16041989</abac:CartaoDataNascimentoTitular>
//             <abac:PreAutorizadaNaPlataforma>true</abac:PreAutorizadaNaPlataforma>
//             <abac:CartaoTID>1111111</abac:CartaoTID>
//             <abac:CartaoNSU>2222222</abac:CartaoNSU>
//             <abac:CartaoNumeroToken>3333333</abac:CartaoNumeroToken>
//             <abac:CodigoTransacaoGateway>001</abac:CodigoTransacaoGateway>
//         </abac:DadosPedidosFormaPgto>
//     </abac:FormasDePagamento>
//     <abac:Itens>
//         <abac:DadosPedidosItem>
//             <abac:CodigoProduto>17228</abac:CodigoProduto>
//             <abac:QuantidadeProduto>1</abac:QuantidadeProduto>
//             <abac:PrecoUnitario>57.81</abac:PrecoUnitario>
//             <abac:EmbalagemPresente>false</abac:EmbalagemPresente>
//             <abac:MensagemPresente/>
//             <abac:PrecoUnitarioBruto>28</abac:PrecoUnitarioBruto>
//             <abac:Brinde>false</abac:Brinde>
//         </abac:DadosPedidosItem>
//     </abac:Itens>
//     <abac:OptouNFPaulista>tbneNao</abac:OptouNFPaulista>
//     <abac:ValorTotalCartaoPresente>0</abac:ValorTotalCartaoPresente>
//     <abac:CartaoPresenteBrinde>false</abac:CartaoPresenteBrinde>
// </abac:DadosPedidos>
		$carrierName = Db::getInstance()->getValue("SELECT `name` FROM `"._DB_PREFIX_."carrier` WHERE `id_carrier` = ".$order->id_carrier);
		$transportadora = null;
		$servicoEntrega = null;
		if(strpos($carrierName, 'Correios') !== false){
			$transportadora = 'Correios';
			$servicoEntrega = (strpos($carrierName, 'PAC') ? 'PAC' : $servicoEntrega);
			$servicoEntrega = (strpos($carrierName, 'SEDEX') ? 'SEDEX' : $servicoEntrega);
			$servicoEntrega = (strpos($carrierName, 'E-SEDEX') ? 'E-SEDEX' : $servicoEntrega);
			$servicoEntrega = (strpos($carrierName, 'Carta Registrada') ? 'Carta Registrada' : $servicoEntrega);
		}

		$condicaoPagamento = NULL;
		$dadosPagamento = array();
		if(strpos(strtolower($order->payment), 'boleto') !== false){

			if (!$boletoDB = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'boletohsbc WHERE id_pedido = '.$order->id)){				
				// Inicia o cURL acessando uma URL
				$cURL = curl_init(_PS_BASE_URL_.__PS_BASE_URI__.'/modules/boletohsbc/gera_boleto.php?order='.$order->id);
				// Define a opção que diz que você quer receber o resultado encontrado
				curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
				// Executa a consulta, conectando-se ao site e salvando o resultado na variável $resultado
				$resultado = curl_exec($cURL);
				// Encerra a conexão com o site
				curl_close($cURL);

				$boletoDB = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'boletohsbc WHERE id_pedido = '.$order->id);
			}

			$dadosPagamento[] = array(
				'FormaPagamentoCodigo' => 'Boleto HSBC',
				'Valor' => number_format($order->total_paid,2),
				'BoletoNumeroBancario' => $boletoDB['nosso_numero'],
				'BoletoVencimento' => date("dmY", strtotime($boletoDB['dt_vencimento'])),
			);
		}elseif(strpos(strtolower($order->payment), 'transferência') !== false){
			$dadosPagamento[] = array(
				// 'FormaPagamentoCodigo' => 'Transferencia',
				'FormaPagamentoCodigo' => 'Transferencia',
				// 'FormaPagamentoCodigo' => 'transferencia',
				'Valor' => number_format($order->total_paid,2),
			);
		}elseif(strpos(strtolower($order->payment), 'mercadopago') !== false){
			$dadosPagamento[] = array(
				'FormaPagamentoCodigo' => 'MercadoPago',
				'Valor' => number_format($order->total_paid,2),
			);
		}

		if($order->total_discounts > 0){
			
			$dadosPagamento[] = array(
				'FormaPagamentoCodigo' => 'Cupom',
				'Valor' => number_format($order->total_discounts,2),
			);
			
		}
		// $condicaoPagamento = (strpos(strtolower($order->payment), 'boleto') !== false ? 'Boleto HSBC' : $condicaoPagamento);;
		// // $condicaoPagamento = (strpos(strtolower($order->payment), 'transferência') !== false ? 'Transferência' : $condicaoPagamento);;
		// $condicaoPagamento = (strpos(strtolower($order->payment), 'transferência') !== false ? 'Transferencia' : $condicaoPagamento);;
		// $condicaoPagamento = (strpos(strtolower($order->payment), 'mercadopago') !== false ? 'MercadoPago' : $condicaoPagamento);;


		$paramsOrder = array(
			'ChavedeIdentificacao' => Configuration::get('abacos_key'),
			'ListaDePedidos' => array (
				'DadosPedidos' => array (
					'RepresentanteVendas' => 1,
					'NumeroDoPedido' => $order->id,
					'EMail' =>  $customer->email,
					'CPFouCNPJ' => preg_replace("/[^0-9]/", "", $customer->document),
					'CodigoCliente' => $customer->id,
					// 'ValorPedido' => number_format($order->total_paid, 2),
					'ValorPedido' => number_format($order->total_products, 2),
					'ValorFrete' => number_format($order->total_shipping, 2),
					'ValorEncargos' =>0,
					// 'ValorDesconto' => number_format($order->total_discounts, 2),
					'ValorEmbalagemPresente' => 0,
 					'DataVenda' => date('dmY' , strtotime($order->date_add)),
					'Transportadora' => $transportadora,
					'ServicoEntrega' => $servicoEntrega,
					
					'Canal'=>'PrestaShop',

					// 'CondicaoPagamento' => $condicaoPagamento,
					'PedidoJaPago' => $order->hasBeenPaid(),
					'DataDoPagamento' => ( $order->valid ) ? date('dmY' , strtotime($order->invoice_date)) : NULL,


//     <abac:DestNome>KPL</abac:DestNome>
//     <abac:DestSexo>tseMasculino</abac:DestSexo>
//     <abac:DestEmail>integracao@kpl.com.br</abac:DestEmail>
//     <abac:DestTelefone>21879090</abac:DestTelefone>
//     <abac:DestLogradouro>Alameda Cauaxi</abac:DestLogradouro>
//     <abac:DestNumeroLogradouro>350</abac:DestNumeroLogradouro>
//     <abac:DestComplementoEndereco>5 andar</abac:DestComplementoEndereco>
//     <abac:DestBairro>Alphaville</abac:DestBairro>
//     <abac:DestMunicipio>Barueri</abac:DestMunicipio>
//     <abac:DestEstado>SP</abac:DestEstado>
//     <abac:DestCep>06454020</abac:DestCep>
//     <abac:DestTipoLocalEntrega>tleeComercial</abac:DestTipoLocalEntrega>
//     <abac:DestEstrangeiro>N</abac:DestEstrangeiro>
//     <abac:DestPais>Brasil</abac:DestPais>
//     <abac:DestCPF>05521031000101</abac:DestCPF>
//     <abac:DestTipoPessoa>tpeJuridica</abac:DestTipoPessoa>

					// 'DestNome' => $customer->firstname.' '.$customer->lastname,
					// 'DestSexo' => ($customer->doc_type == 1) ? 'tseEmpresa' : ($customer->id_gender == 1) ? 'tseMasculino' : 'tseFeminino',
					// 'DestEmail' => $customer->email,
					// 'DestTelefone' => ($addressDelivery->phone) ? $addressDelivery->phone : $addressDelivery->phone_mobile,
					
					// 'Codigo' => $customer->id,
					// 'DestCPF' => preg_replace("/[^0-9]/", "", $customer->document),
					// 'DestTipoPessoa' => preg_replace("/[^0-9]/", "", $customer->document),
					// 'TipoPessoa' => ($customer->doc_type == 2) ? 'tpeFisica' : 'tpeJuridica',
					// 'Documento' => $customer->rg_ie,
					// 'DataNascimento' => date('dmY' , strtotime($customer->birthday)),
					// 'Celular' => $addressDelivery->phone_mobile,
					// 'Endereco' => array(
					// 	'Logradouro' => $addressDelivery->address1,
					// 	'NumeroLogradouro' => $addressDelivery->number,
					// 	'Bairro' => $addressDelivery->address2,
					// 	'Municipio' => $addressDelivery->city,
					// 	'Estado' => $addressDelivery->state,
					// 	'Cep' => preg_replace("/[^0-9]/", "", $addressDelivery->postcode),
					// 	'TipoLocalEntrega' => NULL,
					// 	// 'Pais' => 'Brasil', 
					// ),
					// 'ClienteEstrangeiro' => NULL,
					// 'GrupoCliente' => NULL,
					// 'Classificacao' => 'CONSUMIDOR',
					// 'DataCadastro' => date('dmY' , strtotime($customer->date_add)),


					'Itens' => array (
						'DadosPedidosItem' => $productsABACOS,
					),
					'OptouNFPaulista' => 'tbneNenhum',

					'FormasDePagamento'=> array(
						'DadosPedidosFormaPgto' => $dadosPagamento,
					),

//     <abac:FormasDePagamento>
//         <abac:DadosPedidosFormaPgto>
//             <abac:FormaPagamentoCodigo>Mastercard</abac:FormaPagamentoCodigo>
//             <abac:Valor>57.81</abac:Valor>
//             <abac:CartaoNumero>4012001037141112</abac:CartaoNumero>
//             <abac:CartaoCodigoSeguranca>123</abac:CartaoCodigoSeguranca>
//             <abac:CartaoValidade>122015</abac:CartaoValidade>
//             <abac:CartaoNomeImpresso>KPL Solucoes</abac:CartaoNomeImpresso>
//             <abac:CartaoQtdeParcelas>1</abac:CartaoQtdeParcelas>
//             <abac:CartaoCodigoAutorizacao>4444444444</abac:CartaoCodigoAutorizacao>
//             <abac:CartaoCPFouCNPJTitular>05521031000101</abac:CartaoCPFouCNPJTitular>
//             <abac:CartaoDataNascimentoTitular>16041989</abac:CartaoDataNascimentoTitular>
//             <abac:PreAutorizadaNaPlataforma>true</abac:PreAutorizadaNaPlataforma>
//             <abac:CartaoTID>1111111</abac:CartaoTID>
//             <abac:CartaoNSU>2222222</abac:CartaoNSU>
//             <abac:CartaoNumeroToken>3333333</abac:CartaoNumeroToken>
//             <abac:CodigoTransacaoGateway>001</abac:CodigoTransacaoGateway>
//         </abac:DadosPedidosFormaPgto>
//     </abac:FormasDePagamento>
				),
			),
		);

		$orderABACOS = $webserviceABACOS->insertOrder($paramsOrder);

		if(!$orderABACOS 
			&& !$orderABACOS['InserirPedidoResult'] 
			|| $orderABACOS['InserirPedidoResult']['ResultadoOperacao']['Tipo'] != 'tdreSucesso'
			|| $orderABACOS['InserirPedidoResult']['Rows']['DadosPedidosResultado']['Resultado']['Tipo'] != 'tdreSucesso'
		){
			logMessageAbacos(array(
				'erro ao cadastrar pedido',
				'dataCustomer' => $dataCustomer,
				'insertCustomer' => $insertCustomer,
				'paramsOrder' => $paramsOrder,
				'orderABACOS' => $orderABACOS,
			));
			$msg = "O pedido não foi cadastrado, tente novamente mais tarde";
			if(isset($orderABACOS['InserirPedidoResult']['Rows']['DadosPedidosResultado']['Resultado']['ExceptionMessage'])){
				$msg.= PHP_EOL."<br/>".($orderABACOS['InserirPedidoResult']['Rows']['DadosPedidosResultado']['Resultado']['ExceptionMessage']);
			}
			
			throw new Exception($msg, 404);
			return false;
		}

		$idOrderAbacos = (isset($orderABACOS['InserirPedidoResult']['Rows']['DadosPedidosResultado']['NumeroDoPedido'])) ? $orderABACOS['InserirPedidoResult']['Rows']['DadosPedidosResultado']['NumeroDoPedido'] : $order->id;

		// abacos_order
		Db::getInstance()->insert('abacos_order', array(
			'id_order_abacos' => $idOrderAbacos,
			'id_order_ps' => $order->id,
		));

		return $idOrderAbacos;
	} 
}
 