<?php
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at

//   http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

if (!defined('_PS_VERSION_'))
    exit;

require_once(dirname(__FILE__).'/includes/functions.php');
require_once(dirname(__FILE__).'/includes/webservice.php');

class Abacos extends Module
{
    
    const CHARSET = 'UTF-8';
    
    function __construct()
    {
        $this->name    = 'abacos';
        $this->tab     = 'marketplace';
        $this->version = '1.0';
        $this->author = 'Wing Agency';
        $this->bootstrap = true;
        
        parent::__construct();
        
        $this->displayName = $this->l('ÁBACOS');
        $this->description = $this->l('Modulo de Integração com o ÁBACOS');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->prefix = $this->name.'_';
        $this->html = null;
    }

        public function install() {
        if (
            !parent::install() 
            || !$this->installTabs()
            || !$this->installDb()
            || !$this->registerHook('actionValidateOrder')
			|| !$this->registerHook('actionOrderStatusPostUpdate')
        ){
            return false;
        }

        return true;
    }

    public function uninstall() {
        if (!parent::uninstall() || !$this->uninstallTabs())
            return false;

        return true;
    }

    private function installTabs(){
        $languages = Language::getLanguages();
        $id_parent = Tab::getIdFromClassName('AdminCatalog');
        $tab = new Tab();
        $tab->class_name = 'AdminProductsABACOS';
        $tab->id_parent = $id_parent;
        $tab->module = 'abacos';
        foreach ($languages as $language)
            $tab->name[$language['id_lang']] = 'Produtos ABACOS';
        $tab->add();

        
        $id_parent = Tab::getIdFromClassName('AdminOrders');
        $tab = new Tab();
        $tab->class_name = 'AdminOrdersABACOS';
        $tab->id_parent = $id_parent;
        $tab->module = 'abacos';
        foreach ($languages as $language)
            $tab->name[$language['id_lang']] = 'Pedidos ABACOS';
        return $tab->add();

    }

    private function uninstallTabs(){
        $tabProducts = new Tab(Tab::getIdFromClassName('AdminProductsABACOS'));
         $tabOrders = new Tab(Tab::getIdFromClassName('AdminOrdersABACOS'));
        return ($tabOrders->delete() && $tabProducts->delete())?1:0;
    }

    private function installDb(){
        $DB =  Db::getInstance();

        $DB->execute("CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."abacos_product` (
                `id_abacos_product` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `protocolo` varchar(150) DEFAULT NULL,
                `codigo_produto` varchar(50) DEFAULT NULL,
                `codigo_abacos` int(10) UNSIGNED NOT NULL,
                `data` text,
                `id_product_ps` int(10) UNSIGNED NOT NULL,
                `id_product_attr_ps` int(10) UNSIGNED NOT NULL DEFAULT '0',
                `date_add` datetime DEFAULT CURRENT_TIMESTAMP,
                `date_upd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`id_abacos_product`)  COMMENT '')
                ENGINE = "._MYSQL_ENGINE_."
                DEFAULT CHARACTER SET = utf8;");

        $DB->execute("CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."abacos_order` (
                  `id_abacos_order` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
                  `id_order_abacos` INT(11) NOT NULL COMMENT '',
                  `id_order_ps` INT(10) UNSIGNED NOT NULL COMMENT '',
                  PRIMARY KEY (`id_abacos_order`)  COMMENT '')
                ENGINE = "._MYSQL_ENGINE_."
                DEFAULT CHARACTER SET = utf8;");

        return true;
    }

    private function unninstallDb(){
        $DB =  Db::getInstance();
        $DB->execute("DROP TABLE `"._DB_PREFIX_."abacos_product`");
        $DB->execute("DROP TABLE `"._DB_PREFIX_."abacos_order`");
        return true;
    }


    public function getContent()
    {
        $this->html .= $this->renderForm();

        if (Tools::isSubmit('submitModule'))
        {
            Configuration::updateValue($this->prefix.'key', Tools::getValue($this->prefix.'key'));
            Configuration::updateValue($this->prefix.'url_webservice', Tools::getValue($this->prefix.'url_webservice'));
            Configuration::updateValue($this->prefix.'synchronize_stock', Tools::getValue($this->prefix.'synchronize_stock'));
            Configuration::updateValue($this->prefix.'automatic_order', Tools::getValue($this->prefix.'automatic_order'));
            
        }
        return $this->html;

    }

    private function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                     array(
                        'type' => 'text',
                        'label' => $this->l('URL Webservice'),
                        'name' => $this->prefix.'url_webservice',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Chave de Identificação:'),
                        'name' => $this->prefix.'key',
                        'required' => true,
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Sincronizar Estoque'),
                        'name' => $this->prefix.'synchronize_stock',
                        'desc' => $this->l('Quando marcado como sim irá fazer a sincronização do estoque com o Abacos.'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enviar pedido'),
                        'name' => $this->prefix.'automatic_order',
                        'desc' => $this->l('Quando marcado como sim irá enviar automaticamente os novos pedidos para o abacos.'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                            )
                        ),
                    ),
                    array(
                        'type' => 'html',
                        'name' => 'cron',
                        'label' => '',
                        'col' => '12',
                        'html_content' => '<div class="alert alert-warning text-center" role="alert">Para manter estoque, preço, pedidos atualizado configurar um CRON rodando para as url '
                            .'<br/><strong>'.Tools::getShopDomain(true, true).__PS_BASE_URI__.'modules/abacos/cron/products.php</strong>'
                        	.'<br/><strong>'.Tools::getShopDomain(true, true).__PS_BASE_URI__.'modules/abacos/cron/stock.php</strong>'
                        	.'<br/><strong>'.Tools::getShopDomain(true, true).__PS_BASE_URI__.'modules/abacos/cron/price.php</strong>'
                        	.'<br/><strong>'.Tools::getShopDomain(true, true).__PS_BASE_URI__.'modules/abacos/cron/orderStatus.php</strong>'
                        	.'</div>'
                        	,
                    )

                    
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                )
            ),
        );
        

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table =  $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    private function getConfigFieldsValues()
    {
        $url_webservice = Tools::getValue($this->prefix.'url_webservice');
        $key = Tools::getValue($this->prefix.'key');
        $return = array();

        if (!Validate::isAbsoluteUrl($url_webservice))
            $this->html .= $this->displayError($this->l('URL do webservice inválida, ela deve conter http:// ou https://'));
        else
            $return[$this->prefix.'url_webservice'] = Tools::getValue($this->prefix.'url_webservice', Configuration::get($this->prefix.'url_webservice'));
        
        $return[$this->prefix.'key'] = Tools::getValue($this->prefix.'key', Configuration::get($this->prefix.'key'));
        $return[$this->prefix.'synchronize_stock'] = Tools::getValue($this->prefix.'synchronize_stock', Configuration::get($this->prefix.'synchronize_stock'));
        $return[$this->prefix.'automatic_order'] = Tools::getValue($this->prefix.'automatic_order', Configuration::get($this->prefix.'automatic_order'));
        return $return;
    }



	public function hookActionValidateOrder($params)
	{
		try {
			if (!Configuration::get($this->prefix.'automatic_order') && Db::getInstance()->getValue("SELECT count(0) FROM "._DB_PREFIX_."abacos_order WHERE id_order_ps = '".$idOrder."'") > 0)
				return;

			// Getting differents vars
			$context = Context::getContext();
			$id_lang = (int)$context->language->id;
			$id_shop = (int)$context->shop->id;
			$currency = $params['currency'];
			$order = $params['order'];
			$customer = $params['customer'];


			sendOrderToAbacos($order->id);
		} catch (Exception $e) {
			// $this->errors[] = "Erro ". $e->getCode().': '.$e->getMessage();
			logMessageAbacos(array(
				'Exception' => $e,
				'params' => $params,
			));
		}

	}


	public function hookActionOrderStatusPostUpdate($params)
	{
		$status = $params['newOrderStatus'];
		$idOrder = $params['id_order'];

		try {
			if (!Configuration::get($this->prefix.'automatic_order'))
				return;


			if(Db::getInstance()->getValue("SELECT count(0) FROM "._DB_PREFIX_."abacos_order WHERE id_order_ps = '".$idOrder."'") > 0){
				return;
			}else{
				sendOrderToAbacos($idOrder);
				return;
			}

		} catch (Exception $e) {
			// $this->errors[] = "Erro ". $e->getCode().': '.$e->getMessage();
			logMessageAbacos(array(
				'status' =>$status,
				'idOrder' =>$idOrder,
				'Exception' =>$e,
			));
		}

	}



}