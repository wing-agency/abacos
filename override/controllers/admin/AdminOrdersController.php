<?php

if (!defined('_PS_VERSION_'))
	exit;

class AdminOrdersController extends AdminOrdersControllerCore
{


	public function renderList()
	{
		$this->addRowAction('exportOrderAbacos');
		return parent::renderList();
	}


	public function displayExportOrderAbacosLink($token, $id){
		$html = '';
		if(Db::getInstance()->getValue("SELECT count(0) FROM "._DB_PREFIX_."abacos_order WHERE id_order_ps = '".$id."'") > 0){
			return;
		}
		$href = $this->context->link->getAdminLink('AdminOrdersABACOS').'&id='.$id.'&action=exportOrder';
		$html = '<a href="'.$href.'"title="Exportar"><i class="icon-cloud-upload"></i> Exportar Abacos</a>';
		return $html;
	}

}
