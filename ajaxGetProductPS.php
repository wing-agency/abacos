<?php
require('../../config/config.inc.php');

$search = Tools::getValue('q');

$sql = "SELECT p.id_product, pl.name FROM "._DB_PREFIX_."product AS p
		JOIN "._DB_PREFIX_."product_lang AS pl ON  (p.id_product = pl.id_product AND pl.id_lang = 1)
		WHERE pl.name LIKE '%".$search."%' OR p.reference LIKE '%".$search."%' OR p.ean13 like '%".$search."%' ";
$products = Db::getInstance()->executeS($sql);
$result = array();

if($products && count($products) > 0){
	$result = $products;
}

die(json_encode($result));