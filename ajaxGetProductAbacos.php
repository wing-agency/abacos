<?php

require_once(dirname(dirname(dirname(__FILE__))).'/config/config.inc.php');
require_once(dirname(__FILE__).'/includes/functions.php');
require_once(dirname(__FILE__).'/includes/webservice.php');

error_reporting(E_ALL);
ini_set('display_errors', '1');

$webserviceABACOS = new webserviceABACOS();
$name = isset($_GET['q'])?urldecode($_GET['q']):'';
$products = $webserviceABACOS->productsAvailable();
foreach ($products as $product) {
	if ($product['NomeProduto'] == $name) {
		$return = array();
		if(is_array($products) && count($products) >0){
			$product = array(
				'product_id' => $product['CodigoProduto'],
				'name' => $product['NomeProduto']
				);
			$return[] = $product;
		}
	}
}
header('Content-Type: application/json');
echo json_encode($return);