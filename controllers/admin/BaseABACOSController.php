<?php
require_once dirname(dirname(dirname(__FILE__))).'/includes/functions.php';
require_once dirname(dirname(dirname(__FILE__))).'/includes/webservice.php';

class BaseABACOSController extends ModuleAdminController 
{
	public $webserviceABACOS = null;
	public $module_name = 'abacos';

	public function __construct()
	{
		$this->bootstrap = true;
		$this->lang = true;
		parent::__construct();
	}

	protected function connectionWebservice(){
		$this->webserviceABACOS = new webserviceABACOS();
	}


	public function initPageHeaderToolbar()
	{
		
        $this->page_header_toolbar_btn['config'] = array(
            'href' => 'index.php?controller=AdminModules&token='.Tools::getAdminTokenLite('AdminModules').'&configure=abacos&module_name=abacos',
            'desc' => $this->l('Configuração'),
            'icon' => 'process-icon-cogs'
        );
		parent::initPageHeaderToolbar();
	}
	
}