<?php
require_once __DIR__.'/BaseABACOSController.php';

class AdminProductsABACOSController extends BaseABACOSController 
{

	public function __construct()
	{
		$this->className = 'AdminProductsABACOS';
		$this->identifier = 'CodigoProduto';
		$this->meta_title = $this->l('Produtos ABACOS');
		$this->products_list = array();

		parent::__construct();
		$this->connectionWebservice();

	}

	public function initProcess()
	{

		$this->action = (Tools::getValue('action'))?Tools::getValue('action'):'list';
		switch ($this->action) {
			case 'list':
				$this->displayList();
				break;
			case 'associacaoAbacos':
				$this->associacaoAbacos();
				break;
			case 'updateAssociate':
				$this->updateAssociate();
				break;
			case 'associate':
				$this->associate();
				break;
			default:
				$this->errors[] = "Página não encontrada.";
				$this->displayList();
				break;
		}
		parent::initProcess();
	}

	public function displayList()
	{

		$page = (int)Tools::getValue('submitFilter'.$this->className);
		if (!$page)
			$page = 1;

		$products = $this->webserviceABACOS->productsAvailable();

		$totalProducts = 0;
		$i = 0;
		$codigoProdutos = array();
		if($products && isset($products['products']) && count($products['products'])){
			$totalProducts = count($products['products']);

			if(isset($products['products']['DadosProdutos']['CodigoProduto'])){
				$products['products']['DadosProdutos'] = array($products['products']['DadosProdutos']);
				$totalProducts = count($products['products']);
			}

			foreach ($products['products']['DadosProdutos'] as &$row) {
				$row = $this->prepareProductToList($row);
				// die(debug($row));

				
			}


		}elseif($this->webserviceABACOS->getError()){
			$this->errors[] = $this->webserviceABACOS->getError();
		}


		$fields_list = array();
		$fields_list['CodigoProdutoAbacos'] = array(
			'title' => $this->l('ID'),
			'align' => 'center',
			'width' => 'auto',
			'orderby' => false,
			'filter' => false,
			'search' => false
		);
		$fields_list['CodigoProduto'] = array(
			'title' => $this->l('Código comercial'),
			'align' => 'center',
			'width' => 'auto',
			'orderby' => false,
			'filter' => false,
			'search' => false
		);
		$fields_list['NomeProduto'] = array(
			'title' => $this->l('Nome'),
			'width' => 'auto',
			'orderby' => false,
			'filter' => false,
			'search' => false
		);
		$fields_list['CodigoBarras'] = array(
			'title' => $this->l('EAN'),
			'width' => 'auto',
			'orderby' => false,
			'filter' => false,
			'search' => false
		);
		$fields_list['CodigoFabricante'] = array(
			'title' => $this->l('Código do fabricante'),
			'width' => 'auto',
			'orderby' => false,
			'filter' => false,
			'search' => false
		);
		$fields_list['EstoqueProduto'] = array(
			'title' => $this->l('Estoque'),
			'type' => 'int',
			'align' => 'text-right',
			'orderby' => false,
			'badge_danger' => true,
			'filter' => false,
			'search' => false
		);

		$fields_list['vPreco'] = array(
			'title' => $this->l('Preço de venda'),
			'type' => 'price',
			'align' => 'text-right',
			'filter' => false,
			'search' => false
		);

		$helper = new HelperList();
	    $helper->listTotal= $totalProducts;
	    $helper->shopLinkType = '';
	     
	    $helper->simple_header = false;
	     
	    $helper->title = 'Produtos ÁBACOS';
	    $helper->table = $this->className;
	    $helper->identifier = 'CodigoProduto';


	    // Actions to be displayed in the "Actions" column
	    $helper->actions = array('action');

	    $helper->token = $this->token;
	    $helper->currentIndex = self::$currentIndex;
	    $helper->no_link = true;

	    $this->content .= $helper->generateList($this->products_list, $fields_list);

	}

	private function prepareProductToList(&$row){
		$codigoProdutos[] = $row['CodigoProduto'];

		$associate = Db::getInstance()->getRow("SELECT id_product_ps, id_product_attr_ps, codigo_produto, codigo_abacos, protocolo FROM "._DB_PREFIX_."abacos_product WHERE codigo_abacos = '".$row['CodigoProdutoAbacos'] ."'  AND codigo_produto = '".$row['CodigoProduto']."'");
		if(!$associate){
			$associate = array(
				'id_product_ps'=>0,
				'id_product_attr_ps'=>0, 
				'codigo_produto'=> $row['CodigoProduto'], 
				'codigo_abacos'=> $row['CodigoProdutoAbacos'], 
				'protocolo'=> $row['ProtocoloProduto']
			);
			$dataInsert = $associate;
			$dataInsert['data'] = pSQL(safe_json_encode($row));
			Db::getInstance()->insert('abacos_product', $dataInsert);
		}elseif ($associate['protocolo'] != $row['ProtocoloProduto']){
			$associate['protocolo'] =  $row['ProtocoloProduto'];
			Db::getInstance()->update('abacos_product', array(
				'protocolo'=> $row['ProtocoloProduto'],
				'data'=> pSQL(safe_json_encode($row)),
			), "codigo_abacos = '".$row['CodigoProdutoAbacos'] ."'  AND codigo_produto = '".$row['CodigoProduto']."'");

			if($associate['id_product_ps'] > 0){
				sendproductConfirmAtion($row['CodigoProduto']);

				$product = new Product((int)$associate['id_product_ps']);
				if($product->id){
					if($row['Acao'] == 'aieExclusao'){
						$product->active = false;
					}else{
						$product->active = true;
					}
					$product->update();
				}
			}
		}

		$row['associate'] = $associate;

		$row['CustoDoProduto'] = convertPriceBrlToUsd($row['CustoDoProduto']);
		// $row['EstoqueProduto'] = $this->webserviceABACOS->stockBalanceOnline($row['CodigoProduto']);
		// $row['EstoqueProduto'] = isset($row['EstoqueProduto']) ? $row['EstoqueProduto']['SaldoDisponivel']: false;
		$row['EstoqueProduto'] = 0;
		$row['CodigoProduto'] = $row['CodigoProduto'];

		$this->products_list[$row['CodigoProduto']] = $row;

		return $row;
	}


	public function displayActionLink($token, $id){
		$href = $this->context->link->getAdminLink('AdminProductsABACOS').'&id='.$id.'&action=';
		$html = '';
		if($this->products_list[$id]['associate']['id_product_ps'] == 0){
			$html = '<a href="'.$href.'associate" title="Associar Produto" class=" btn btn-default"><i class="icon-compress"></i> Associar Produto</a>';
		}elseif($this->products_list[$id]['associate']['id_product_ps'] > 0){
			$html = '<a href="'.$href.'updateAssociate" title="Atualizar Produto" class=" btn btn-default"><i class="icon-refresh"></i> Atualizar Produto</a>';
		}
		return $html;
	}

	public function associate(){
		$codigoProduto = Tools::getValue('id');
		$dataProduct = Db::getInstance()->getValue("SELECT data FROM "._DB_PREFIX_."abacos_product WHERE  codigo_produto = '".$codigoProduto."'");

		if(!$dataProduct){
			$this->errors[] = "Produto não encontrado";
			return;
		}

		if(Tools::isSubmit('submitAssociate')){
			$id_lang = $this->context->language->id;
			$idProduct = (int) Tools::getValue('id_product_ps');
			$idProductAttribute = (int) Tools::getValue('id_product_attribute');
			if($idProduct > 0){
				$product = new Product($idProduct, true, $id_lang);
				if($product->id == $idProduct){
					$attributes = $product->getAttributesResume($id_lang);

					if(!$attributes || $idProductAttribute > 0){

						Db::getInstance()->update('abacos_product', array(
							'id_product_ps'=> $idProduct,
							'id_product_attr_ps'=> $idProductAttribute,
							'data'=> pSQL($dataProduct),
						), "codigo_produto = '".$codigoProduto ."'");

						if(sendproductConfirmAtion($codigoProduto)){
							Tools::redirectAdmin('index.php?controller=AdminProductsABACOS&token='.Tools::getAdminTokenLite('AdminProductsABACOS'));
						}else{
							Tools::redirectAdmin('index.php?controller=AdminProductsABACOS&id='.$codigoProduto.'&action=associate&token='.Tools::getAdminTokenLite('AdminProductsABACOS'));
						}

					}else{
						$dataProduct = json_decode($dataProduct,true);

						$tpl = $this->createTemplate('associateAttributes.tpl');

						$tpl->assign('id', $dataProduct['CodigoProduto']);
						$tpl->assign('product', array(
							'name'=>$dataProduct['NomeProduto'],
							'ean'=>$dataProduct['CodigoBarras'],
							'codigoProduto'=>$dataProduct['CodigoProduto'],
							'codigoProdutoAbacos'=>$dataProduct['CodigoProdutoAbacos'],
							'id_product_ps'=>$idProduct,
							'attributes'=>$attributes,
						));

						$this->content .= $tpl->fetch();

					}

					return;

				}
			}
			$this->errors[] = "Não foi possível associar o produto. Produto não encontrado";
		}

		$dataProduct = json_decode($dataProduct,true);

		$tpl = $this->createTemplate('associate.tpl');

		$tpl->assign('urlAjaxGetProduct', Tools::getShopDomainSsl(true).__PS_BASE_URI__ .'modules/abacos/ajaxGetProductPS.php');
		$tpl->assign('id', $dataProduct['CodigoProduto']);
		$tpl->assign('product', array(
			'name'=>$dataProduct['NomeProduto'],
			'ean'=>$dataProduct['CodigoBarras'],
			'codigoProduto'=>$dataProduct['CodigoProduto'],
			'codigoProdutoAbacos'=>$dataProduct['CodigoProdutoAbacos'],
		));

		$this->content .= '<link href="'.__PS_BASE_URI__.'js/jquery/plugins/autocomplete/jquery.autocomplete.css" rel="stylesheet" type="text/css" media="all" />
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/plugins/autocomplete/jquery.autocomplete.js"></script>';

		$this->content .= $tpl->fetch();
	}

	public function  updateAssociate(){
		$id_lang = $this->context->language->id;
		$codigoProduto = Tools::getValue('id');
		$dataProduct = Db::getInstance()->getValue("SELECT data FROM "._DB_PREFIX_."abacos_product WHERE  codigo_produto = '".$codigoProduto."'");

		if(sendproductConfirmAtion($codigoProduto)){
			Tools::redirectAdmin('index.php?controller=AdminProductsABACOS&token='.Tools::getAdminTokenLite('AdminProductsABACOS'));
		}else{
			Tools::redirectAdmin('index.php?controller=AdminProductsABACOS&id='.$codigoProduto.'&action=associate&token='.Tools::getAdminTokenLite('AdminProductsABACOS'));
		}
		$this->content .= debug(array($codigoProduto, sendproductConfirmAtion($codigoProduto)));

	}


	public function associacaoAbacos(){
		$id_lang = $this->context->language->id;
		$idProduct = Tools::getValue('id_product');
		$product = new Product($idProduct, true, $id_lang);
		if(!$product->id){
			$this->errors[] = $this->l("Produto não encontrado");
			return;
		}

		$attributes = $product->getAttributesResume($id_lang);

		$DB = Db::getInstance();
		if(Tools::isSubmit('submitAssociacaoAbacos')){
			if(Tools::getValue('abacos_codigo_abacos') == '' && Tools::getValue('abacos_codigo_produto') == ''){
				$DB->delete("abacos_product", "id_product_ps = ".$idProduct. " AND id_product_attr_ps = 0");
			}else{
				if($DB->getValue("SELECT id_abacos_product FROM `"._DB_PREFIX_."abacos_product` WHERE id_product_ps = ".$idProduct. " AND id_product_attr_ps = 0")){
					$DB->update('abacos_product', array(
						'id_product_attr_ps' => 0,
						'id_product_ps' => $idProduct,
						'codigo_abacos' => pSQL(Tools::getValue('abacos_codigo_abacos')),
						'codigo_produto' => pSQL(Tools::getValue('abacos_codigo_produto')),
					),"id_product_ps = ".$idProduct. " AND id_product_attr_ps = 0");
				}else{
					$DB->insert('abacos_product', array(
						'id_product_attr_ps' => 0,
						'id_product_ps' => $idProduct,
						'codigo_abacos' => pSQL(Tools::getValue('abacos_codigo_abacos')),
						'codigo_produto' => pSQL(Tools::getValue('abacos_codigo_produto')),
					));
				}
			}

			if($attributes && count($attributes) > 0){
				foreach ($attributes as $row){
					$idAttribute = $row['id_product_attribute'];
					if(Tools::getValue('abacos_codigo_abacos-'.$idAttribute) == '' && Tools::getValue('abacos_codigo_produto-'.$idAttribute) == ''){
						$DB->delete("abacos_product", "id_product_ps = ".$idProduct. " AND id_product_attr_ps = ".$idAttribute);
					}else{
						if($DB->getValue("SELECT id_abacos_product FROM `"._DB_PREFIX_."abacos_product` WHERE id_product_ps = ".$idProduct." AND id_product_attr_ps =".$idAttribute)){
							$DB->update('abacos_product', array(
								'id_product_attr_ps' => $idAttribute,
								'id_product_ps' => $idProduct,
								'codigo_abacos' => pSQL(Tools::getValue('abacos_codigo_abacos-'.$idAttribute)),
								'codigo_produto' => pSQL(Tools::getValue('abacos_codigo_produto-'.$idAttribute)),
							),"id_product_ps = ".$idProduct. " AND id_product_attr_ps = ".$idAttribute);
						}else{
							$DB->insert('abacos_product', array(
								'id_product_attr_ps' => $idAttribute,
								'id_product_ps' => $idProduct,
								'codigo_abacos' => pSQL(Tools::getValue('abacos_codigo_abacos-'.$idAttribute)),
								'codigo_produto' => pSQL(Tools::getValue('abacos_codigo_produto-'.$idAttribute)),
							));
						}
					}
				}
			}
		}


		$formFields = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Produto: ').$product->name.' - ' .($product->reference!= '' ? 'REF: '.$product->reference.' ' :'').($product->ean13 != ''? 'EAN: '.$product->ean13 :'' ),
					'icon' => 'icon-tags'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Código Produto'),
						'name' => 'abacos_codigo_produto',
						'required' => true
					),
					array(
						'type' => 'text',
						'label' => $this->l('Código Abacos:'),
						'name' => 'abacos_codigo_abacos',
						'required' => true,
					),
				),
				'submit' => array(
					'title' => $this->l('Save')
				)
			),
		);

		$sql = "SELECT id_product_attr_ps, codigo_produto, codigo_abacos FROM `"._DB_PREFIX_."abacos_product` WHERE id_product_ps = ".$idProduct;
		$combinacoes = array();
		if ($results = Db::getInstance()->ExecuteS($sql)){
			foreach ($results as $row){
				$combinacoes[$row['id_product_attr_ps']] = $row;
			}
		}

		$formValues = array(
			'abacos_codigo_produto'=> isset($combinacoes[0])?$combinacoes[0]['codigo_produto'] : '',
			'abacos_codigo_abacos'=> isset($combinacoes[0])?$combinacoes[0]['codigo_abacos'] : '',
		);


		if($attributes && count($attributes) > 0){
			foreach ($attributes as $row) {
				
				$idAttribute = $row['id_product_attribute'];
				$formFields['form']['input'][] = array(
					'type' => 'html',
					'name' => 'attribute_'.$idAttribute,
					'label' => '',
					'col' => '12',
					'html_content' => '<legend><smal>'.$this->l('Combinação: ').$row['attribute_designation'].'</smal></legend>',
				);
				$formFields['form']['input'][] = array(
					'type' => 'text',
					'label' => $this->l('Código Produto:'),
					'name' => 'abacos_codigo_produto-'.$idAttribute,
					'required' => true,
				);
				$formFields['form']['input'][] = array(
					'type' => 'text',
					'label' => $this->l('Código Abacos:'),
					'name' => 'abacos_codigo_abacos-'.$idAttribute,
					'required' => true,
				);

				$formValues['abacos_codigo_produto-'.$idAttribute] = isset($combinacoes[$idAttribute])?$combinacoes[$idAttribute]['codigo_produto'] : '';
				$formValues['abacos_codigo_abacos-'.$idAttribute] = isset($combinacoes[$idAttribute])?$combinacoes[$idAttribute]['codigo_abacos'] : '';
			}
		}



		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  '';
		$helper->default_form_language = $id_lang;

		$helper->identifier = $this->identifier;

		$helper->submit_action = 'submitAssociacaoAbacos';

		$helper->currentIndex = $this->context->link->getAdminLink($this->className, false).'&action=associacaoAbacos&id_product='.$idProduct;
		$helper->token = Tools::getAdminTokenLite($this->className);
		$helper->tpl_vars = array(
			'fields_value' => $formValues,
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		$this->content .= $helper->generateForm(array($formFields));
		// $this->content .= debug($attributes);

	}


}