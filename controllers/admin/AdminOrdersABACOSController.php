<?php
require_once __DIR__.'/BaseABACOSController.php';

class AdminOrdersABACOSController extends BaseABACOSController 
{
	public function __construct()
	{
		$this->className = 'AdminOrdersABACOS';
		$this->meta_title = $this->l('Pedidos ABACOS');

		parent::__construct();

	}
	
	public function initProcess()
	{

		$this->action = (Tools::getValue('action'))?Tools::getValue('action'):'list';
		switch ($this->action) {
			case 'list':
				$this->displayList();
				break;
			case 'exportOrder':
				$this->exportOrder();
				break;
			default:
				$this->displayList();
				break;
		}
		parent::initProcess();
	}


	public function displayList()
	{
	}

	private function exportOrder()
	{

		$idOrder = intval(Tools::getValue('id'));
		try {
			sendOrderToAbacos($idOrder);

			// https://www.navigandi.com.br/admin0957/index.php?controller=AdminOrders&id_order=4975&vieworder&token=833bd509072ea5b7ebd0d8a3e3e37950
			Tools::redirectAdmin('index.php?controller=AdminOrders&id_order='.$idOrder.'&vieworder&token='.Tools::getAdminTokenLite('AdminOrders'));

		} catch (Exception $e) {
			$this->errors[] = "Erro ". $e->getCode().': '.$e->getMessage();
		}
	}
}