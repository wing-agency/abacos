<?php
require_once __DIR__.'/BaseABACOSController.php';

class AdminProductsABACOSController extends BaseABACOSController 
{

	public function __construct()
	{
		$this->className = 'AdminProductsABACOS';
		$this->identifier = 'CodigoProduto';
		$this->meta_title = $this->l('Produtos ABACOS');
		$this->products_list = array();

		parent::__construct();
		$this->connectionWebservice();

	}

	public function initProcess()
	{

	    $this->action = (Tools::getValue('action'))?Tools::getValue('action'):'list';
	    switch ($this->action) {
			case 'list':
				$this->displayList();
				break;
			case 'importProduct':
				$this->importProduct();
				break;
			case 'removeAssociateProduct':
				$this->removeAssociateProduct();
				break;
			case 'associateProduct':
				$this->associateProduct();
				break;
			case 'synchronizeProduct':
				$this->synchronizeProduct();
				break;
			default:
				$this->displayList();
				break;
		}
	    parent::initProcess();
	}


	public function displayList()
	{

		$page = (int)Tools::getValue('submitFilter'.$this->className);
		if (!$page)
			$page = 1;

		$this->setParamsSearch();
		$rProd = (int)$this->context->cookie->{$this->className.'Filter_rProd'};
		$iCodBrand = (int)$this->context->cookie->{$this->className.'Filter_cLin'};
		$iCodGrupo = (int)$this->context->cookie->{$this->className.'Filter_cGrup'};
		$iNrPac = $page;
		$iQtdePac = $this->context->cookie->{$this->className.'_pagination'};
		$sDataHora = null;

		$products = $this->webserviceABACOS->productsAvailable();
		$totalProducts = 0;
		$i = 0;
		$codigoProdutos = array();
		if($products && isset($products['products']) && count($products['products'])){
			$totalProducts = count($products['products']);

			foreach ($products['products']['DadosProdutos'] as &$row) {
				$codigoProdutos[] = $row['CodigoProduto'];
				$row['CustoDoProduto'] = convertPriceBrlToUsd($row['CustoDoProduto']);
				$row['EstoqueProduto'] = $this->webserviceABACOS->stockBalanceOnline($row['CodigoProduto']);

				if($i == 0){
					$this->content .= debug($row['EstoqueProduto']);
					$i++;
				}
				$row['EstoqueProduto'] = isset($row['EstoqueProduto']) ? $row['EstoqueProduto']['SaldoDisponivel']: false;

				// $row['EstoqueProduto'] = intval($this->webserviceABACOS->stockBalanceOnline($row['CodigoProduto']));
				// $row['EstoqueProduto'] = 0;
				$row['CodigoProduto'] = $row['CodigoProduto'];

				// $select = Db::getInstance()->getValue("SELECT id_product_ps FROM "._DB_PREFIX_."abacos_product WHERE id_product_abacos = '".$row['CodigoProduto']."'");
				// $row['associate'] = intval($select);
				$row['associate'] = false;

				$this->products_list[$row['CodigoProduto']] = $row;
			}


		}elseif($this->webserviceABACOS->getError()){
			$this->errors[] = $this->webserviceABACOS->getError();
		}


		$fields_list = array();
		$fields_list['CodigoProdutoAbacos'] = array(
			'title' => $this->l('ID'),
			'align' => 'center',
			'width' => 'auto',
			'orderby' => false,
			'filter' => false,
			'search' => true
		);
		$fields_list['CodigoProduto'] = array(
			'title' => $this->l('Código comercial'),
			'align' => 'center',
			'width' => 'auto',
			'orderby' => false,
			'filter' => false,
			'search' => false
		);
		$fields_list['NomeProduto'] = array(
			'title' => $this->l('Nome'),
			'width' => 'auto',
			'orderby' => false,
			'filter' => false,
			'search' => false
		);
		$fields_list['CodigoBarras'] = array(
			'title' => $this->l('EAN'),
			'width' => 'auto',
			'orderby' => false,
			'filter' => false,
			'search' => false
		);
		$fields_list['EstoqueProduto'] = array(
			'title' => $this->l('Estoque'),
			'type' => 'int',
			'align' => 'text-right',
			'orderby' => false,
			'badge_danger' => true,
			'filter' => false,
			'search' => false
		);

		$fields_list['vPreco'] = array(
			'title' => $this->l('Preço de venda'),
			'type' => 'price',
			'align' => 'text-right',
			'filter' => false,
			'search' => false
		);

		// $fields_list['xLin'] = array(
		// 	'title' => $this->l('Linha'),
		// 	'width' => 'auto',
		// 	'orderby' => false,
		// 	'filter' => false,
		// 	'search' => false
		// );
		// $fields_list['cGrup'] = array(
		// 	'title' => $this->l('Grupo'),
		// 	'width' => 'auto',
		// 	'orderby' => false,
		// 	'filter' => false,
		// 	'search' => false
		// );
		// $fields_list['xLin'] = array(
		// 	'title' => $this->l('Linha'),
		// 	'filter_key' => 'cLin',
		// 	// 'active' => 'active',
		// 	'align' => 'text-center',
		// 	'type' => 'select',
		// 	'list'=> $brands,
		// 	'orderby' => false
		// );
		// $fields_list['xGrup'] = array(
		// 	'title' => $this->l('Grupo'),
		// 	'filter_key' => 'cGrup',
		// 	// 'active' => 'active',
		// 	'align' => 'text-center',
		// 	'type' => 'select',
		// 	'list'=> $groups,
		// 	'orderby' => false
		// );


		// $fields_list['Atv'] = array(
		// 	'title' => $this->l('Ativo'),
		// 	'filter_key' => 'cLin',
		// 	'active' => 's',
		// 	'align' => 'text-center',
		// 	'type' => 'select',
		// 	'list'=> array('s'=>'Sim','n'=>'Não'),
		// 	'orderby' => false,
		// 	'filter' => false,
		// 	'search' => false
		// );
		

		$helper = new HelperList();
	    $helper->listTotal= $totalProducts;
	    $helper->shopLinkType = '';
	     
	    $helper->simple_header = false;
	     
	    $helper->title = 'Produtos ÁBACOS';
	    $helper->table = $this->className;
	    $helper->identifier = 'rProd';


	    // Actions to be displayed in the "Actions" column
	    $helper->actions = array('importProduct','associate');

	    $helper->token = $this->token;
	    $helper->currentIndex = self::$currentIndex;
	    $helper->no_link = true;
	    $helper->tpl_vars =array(
	    	'order_by'=>$this->context->cookie->{$this->className.'Orderby'},
	    	'order_way'=>$this->context->cookie->{$this->className.'Orderway'},
	    	);

	    $this->content .= $helper->generateList($this->products_list, $fields_list);
	    $this->content .= debug($products['products']);

	}

	private function setParamsSearch(){
		if(Tools::isSubmit('submitReset'.$this->className)){
			$this->processResetFilters($this->className);
			$this->context->cookie->{$this->className.'_pagination'} = 20;
			unset($this->context->cookie->{$this->className.'Filter_cLin'});
			unset($this->context->cookie->{$this->className.'Filter_cGrup'});
			unset($this->context->cookie->{$this->className.'Filter_rProd'});
			unset($this->context->cookie->{$this->className.'Orderby'});
			unset($this->context->cookie->{$this->className.'Orderway'});
			Tools::redirectAdmin(AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite($this->className));
			die();
		}else{
			if(Tools::getValue($this->className.'_pagination')){
				$this->context->cookie->{$this->className.'_pagination'} = Tools::getValue($this->className.'_pagination');
			}
			if(Tools::getValue($this->className.'Filter_cLin')){
				$this->context->cookie->{$this->className.'Filter_cLin'} = Tools::getValue($this->className.'Filter_cLin');
			}
			if(Tools::getValue($this->className.'Filter_cGrup')){
				$this->context->cookie->{$this->className.'Filter_cGrup'} = Tools::getValue($this->className.'Filter_cGrup');
			}
			if(Tools::getValue($this->className.'Filter_rProd')){
				$this->context->cookie->{$this->className.'Filter_rProd'} = Tools::getValue($this->className.'Filter_rProd');
			}
		}
		if(!$this->context->cookie->{$this->className.'_pagination'}){
			$this->context->cookie->{$this->className.'_pagination'} = 20;
		}

	}

	public function displayImportProductLink($token, $id){

    	if(!$this->products_list[$id]['associate']){
	    	$tpl = $this->context->smarty->createTemplate($this->getTemplatePath().'list_action.tpl');

		    $tpl->assign(array(
		        'href' => self::$currentIndex.'&token='.$this->token.'&'.$this->identifier.'='.$id.'&action=importProduct',
		        'icon' => 'icon-cloud-download',
	            'action' => $this->l('Importar')
		    ));
	    	return $tpl->fetch();
    	}else{
	    	$tpl = $this->context->smarty->createTemplate($this->getTemplatePath().'list_action.tpl');

		    $tpl->assign(array(
		        'href' => self::$currentIndex.'&token='.$this->token.'&'.$this->identifier.'='.$id.'&action=synchronizeProduct',
		        'icon' => 'icon-refresh',
	            'action' => $this->l('Sincronizar')
		    ));
	    	return $tpl->fetch();

    	}
    	return NULL;
	}

	public function displayAssociateLink($token, $id){

    	if(!$this->products_list[$id]['associate']){
	    	$tpl = $this->context->smarty->createTemplate($this->getTemplatePath().'list_action.tpl');

		    $tpl->assign(array(
		        'href' => self::$currentIndex.'&token='.$this->token.'&'.$this->identifier.'='.$id.'&name='.$this->products_list[$id]['ProdutoNome'].'&action=associateProduct',
		        'icon' => 'icon-compress',
	            'action' => $this->l('Associar')
		    ));
	    	return $tpl->fetch();
    	}else{

	    	$tpl = $this->context->smarty->createTemplate($this->getTemplatePath().'list_action.tpl');

		    $tpl->assign(array(
		        'href' => self::$currentIndex.'&token='.$this->token.'&'.$this->identifier.'='.$id.'&action=removeAssociateProduct',
		        'icon' => 'icon-remove',
	            'action' => $this->l('Remover Asso.')
		    ));
	    	return $tpl->fetch();
    	}
    	return NULL;
	}

	

	public function importProduct(){
		$id_lang = $this->context->language->id;
		$productABACOS = array(); 
		$productExists = $this->webserviceABACOS->productExists(intval(Tools::getValue('rProd')));
		if ($productExists != 0) {
			$productsABACOS = $this->webserviceABACOS->productsAvailable();
			foreach ($productsABACOS as $product) {
				if ( $product['CodigoProduto'] == intval(Tools::getValue('rProd')) ) {
					$productABACOS['product'] = $product;
					$productABACOS['product']['ProdutoAtivo'] = $productExists['ProdutoAtivo'];
				} else {
					$this->errors[] = "O Produto com ID ".Tools::getValue('rProd')." não foi encontrado no ABACOS";
				}
			}
		}
		if($this->webserviceABACOS->getError()){
			$this->errors[] = $this->webserviceABACOS->getError();
		}else{
			$productPS = new Product();

			$productPS->name[$id_lang] = ucfirst(strtolower($productABACOS['product']['NomeProduto']));
			$productPS->link_rewrite[$id_lang] = Tools::link_rewrite($productPS->name[$id_lang]);

			$productPS->width = $productABACOS['product']['Largura'];
			$productPS->height = $productABACOS['product']['Altura'];
			$productPS->depth = $productABACOS['product']['Comprimento'];
			$productPS->weight = $productABACOS['product']['Peso'];

			$productPS->reference = $productABACOS['product']['CodigoProduto'];
			$productPS->wholesale_price = convertPriceBrlToUsd($productABACOS['product']['Custo']);
			$productPS->price = convertPriceBrlToUsd($this->webserviceABACOS->priceOnline($productABACOS['product']['CodigoProduto']));
			$productPS->id_tax_rules_group = 0;
			$productPS->quantity = $this->webserviceABACOS->stockBalanceOnline($productABACOS['product']['CodigoProduto']);

			$rootCategory = Category::getRootCategory($id_lang);

			$productPS->id_category_default = $rootCategory->id;
			$productPS->id_manufacturer = $productABACOS['product']['CodigoFabricante'];
			$productPS->new = true;
			$productPS->available_date = date('Y-m-d');
			$productPS->active = $productABACOS['product']['ProdutoAtivo'];

			$productPS->add();

			if($productPS->id){
				StockAvailable::setQuantity($productPS->id, 0, intval($this->webserviceABACOS->stockBalanceOnline($productABACOS['product']['CodigoProduto'])));
				$productPS->updateCategories(array($rootCategory->id));
				if(saveRelationERP(intval(Tools::getValue('rProd')), $productPS->id)){

					Tools::redirectAdmin('index.php?controller=AdminProducts&id_product='.$productPS->id.'&updateproduct&token='.Tools::getAdminTokenLite('AdminProducts'));
					die();
				}else{
					$this->errors[] = 'Erro ao salvar a associação produto';
					$this->errors[] =Db::getInstance()->getMsgError();
					logMessage($this->errors,3, dirname(dirname(dirname(__FILE__))).'/logs/log.log');
				}
			}else{
				$this->errors[] = 'Erro ao importar produto';
				$this->errors[] =Db::getInstance()->getMsgError();
				logMessage($this->errors,3, dirname(dirname(dirname(__FILE__))).'/logs/log.log');
			}
		}
	}

	private function removeAssociateProduct()
	{
		$idProductABACOS = intval(Tools::getValue('rProd'));
		if(Db::getInstance()->delete('abacos_product', 'id_product_abacos ='.$idProductABACOS, 1)){
			Tools::redirectAdmin(AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite($this->className));
			die();
		}{
			$this->errors[] = 'Erro ao remover associação do produto'.$idProductABACOS;
			$this->errors[] = Db::getInstance()->getMsgError();
			logMessage($this->errors,3, dirname(dirname(dirname(__FILE__))).'/logs/log.log');
		}
	}

	private function associateProduct()
	{
		$idProductABACOS = intval(Tools::getValue('rProd'));
		$name = (Tools::getValue('name') != '') ? Tools::getValue('name'):false;

		if (Tools::isSubmit('submitAssociate')) {
			$idProductPS = intval(Tools::getValue('id_product_ps'));
			$product = new Product($idProductPS);
			if(!$product->id){
				$this->errors[] = 'Produto com id '.$idProductPS.' não foi encontrado no PrestaShop';
			}else{
				$productABACOS = array(); 
				$productExists = $this->webserviceABACOS->productExists($idProductABACOS);
				if ($productExists != 0) {
					$productsABACOS = $this->webserviceABACOS->productsAvailable();
					foreach ($productsABACOS as $product) {
						if ( $product['CodigoProduto'] == $idProductABACOS) {
							$productABACOS['product'] = $product;
							$productABACOS['product']['ProdutoAtivo'] = $productExists['ProdutoAtivo'];
						} else {
							$this->errors[] = "O Produto com ID ".$idProductABACOS." não foi encontrado no ABACOS";
						}
					}
				}
				if(count($product) < 1){
					$this->errors[] = 'Produto com id '.$idProductABACOS.' não foi encontrado no Webservice';
				}else{

					$product->reference = $productABACOS['product']['CodigoProduto'];
					//$idManufacturer =  Manufacturer::getIdByName($productABACOS['product']['CodigoFabricante']);
					$product->id_manufacturer = $productABACOS['product']['CodigoFabricante'];
					$product->new = true;
					$product->active = $productABACOS['product']['ProdutoAtivo'];

					if(intval(Tools::getValue('update_price'))){
						$product->wholesale_price = convertPriceBrlToUsd($productABACOS['product']['Custo']);
						$product->price = convertPriceBrlToUsd($this->webserviceABACOS->priceOnline($productABACOS['product']['CodigoProduto']));
					}
					if(intval(Tools::getValue('update_stock'))){
						StockAvailable::setQuantity($product->id, 0, intval($this->webserviceABACOS->stockBalanceOnline($productABACOS['product']['CodigoProduto'])));
					}

					if (!$product->update()){
						$this->errors[] = 'Erro ao atualizar dados do produto';
						$this->errors[] = Db::getInstance()->getMsgError();
						logMessage($this->errors,3, dirname(dirname(dirname(__FILE__))).'/logs/log.log');
					}else{
						if(saveRelationERP($idProductABACOS, $product->id)){

							Tools::redirectAdmin('index.php?controller=AdminProducts&id_product='.$product->id.'&updateproduct&token='.Tools::getAdminTokenLite('AdminProducts'));
							die();
						}else{
							$this->errors[] = 'Erro ao salvar a associação produto';
							$this->errors[] =Db::getInstance()->getMsgError();
							logMessage($this->errors,3, dirname(dirname(dirname(__FILE__))).'/logs/log.log');
						}
					}
				}
			}
		}

    	$tpl = $this->createTemplate('link_product.tpl');
		$tpl->assign('urlAjaxGetProductML', __PS_BASE_URI__.'modules/abacos/ajaxGetProductPS.php');
		$tpl->assign('idProductABACOS', $idProductABACOS);
		$tpl->assign('name', $name);
		$this->content .= '<link href="'.__PS_BASE_URI__.'js/jquery/plugins/autocomplete/jquery.autocomplete.css" rel="stylesheet" type="text/css" media="all" />
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/plugins/autocomplete/jquery.autocomplete.js"></script>';
		$this->content .= $tpl->fetch();
	}

	private function synchronizeProduct()
	{
		$idProductABACOS = intval(Tools::getValue('rProd'));
		$productExists = $this->webserviceABACOS->productExists($idProductABACOS);
		if ($productExists != 0) {
			$productsABACOS = $this->webserviceABACOS->productsAvailable();
			foreach ($productsABACOS as $product) {
				if ( $product['CodigoProduto'] == $idProductABACOS ) {
					$productABACOS['product'] = $product;
					$productABACOS['product']['ProdutoAtivo'] = $productExists['ProdutoAtivo'];
				} else {
					$this->errors[] = "O Produto com ID ".$idProductABACOS." não foi encontrado no ABACOS";
				}
			}
		}
		$idProductPS = (int)Db::getInstance()->getValue("SELECT id_product_ps FROM "._DB_PREFIX_."abacos_product WHERE id_product_abacos = '".$idProductABACOS."'");
		if(!$productABACOS || count($productABACOS) < 1){
			$productABACOS = false;
			$this->errors[] = 'Erro ao buscar dados do produto no webservice';
			if($this->webserviceABACOS->getError()){
				$this->errors[] = $this->webserviceABACOS->getError();
			}
			logMessage($this->errors,3, dirname(dirname(dirname(__FILE__))).'/logs/log.log');
		}
		$productPS = false;
		if($idProductPS){
			$productPS = new Product($idProductPS);
			if($productPS->id != $idProductPS){
				$productPS = false;
				$this->errors[] = 'O produto ('.$idProductPS.') associado não foi encontrado';
				logMessage($this->errors,3, dirname(dirname(dirname(__FILE__))).'/logs/log.log');
			}
		}else{
			$this->errors[] = 'Não encontramos nenhum produto no site associado ao produto do ABACOS com id '.$idProductABACOS;
			logMessage($this->errors,3, dirname(dirname(dirname(__FILE__))).'/logs/log.log');
		}

		if ($productABACOS && $productPS){

			if (Tools::isSubmit('submitSynchronizeProduct')) {
				$this->content.= debug('teste');
				$error = false;
				if(intval(Tools::getValue('update_price'))){
					$productPS->wholesale_price = convertPriceBrlToUsd($productABACOS['product']['Custo']);
					$productPS->price = convertPriceBrlToUsd($this->webserviceABACOS->priceOnline($productABACOS['product']['CodigoProduto']));
					if(!$productPS->update()){
						$error = true;
						$this->errors[] = 'Erro ao atualizar preço do produto';
						$this->errors[] = Db::getInstance()->getMsgError();
						logMessage($this->errors,3, dirname(dirname(dirname(__FILE__))).'/logs/log.log');
					}
				}
				if(intval(Tools::getValue('update_stock'))){
					if(StockAvailable::setQuantity($productPS->id, 0, intval($this->webserviceABACOS->stockBalanceOnline($productABACOS['product']['CodigoProduto'])))){
						$error = true;
						$this->errors[] = 'Erro ao atualizar estoque do produto';
						$this->errors[] = Db::getInstance()->getMsgError();
						logMessage($this->errors,3, dirname(dirname(dirname(__FILE__))).'/logs/log.log');
					}
				}
				if(!$error){
					Tools::redirectAdmin('index.php?controller=AdminProducts&id_product='.$productPS->id.'&updateproduct&token='.Tools::getAdminTokenLite('AdminProducts'));
					die();
				}
			}

			$tpl = $this->createTemplate('synchronize_product.tpl');
			$tpl->assign('idProductABACOS', $productABACOS['product']['CodigoProduto']);
			$tpl->assign('name', $productABACOS['product']['NomeProduto']);
			$tpl->assign('priceABACOS', convertPriceBrlToUsd($this->webserviceABACOS->priceOnline($productABACOS['product']['CodigoProduto'])));
			$tpl->assign('pricePS', $productPS->price);
			$tpl->assign('quantityABACOS', $this->webserviceABACOS->stockBalanceOnline($productABACOS['product']['CodigoProduto']));
			$tpl->assign('quantityPS', StockAvailable::getQuantityAvailableByProduct($productPS->id,0));
			$this->content .= $tpl->fetch();
		}
		return;

	}
}