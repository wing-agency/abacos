<form method="post" action="index.php?controller={$smarty.get.controller}&token={$smarty.get.token}&id={$id}&action=associate"  class="form-horizontal defaultForm">

	<input type="hidden" id="id_product" name="id_product_ps" value="{{$product.id_product_ps}}" />

	<div id="fieldset_0" class="panel">
		<div class="panel-heading">Associação - Produto </div>
		<div class="form-wrapper">
			<div class="form-group">
				<label class="control-label col-lg-3 required" for="id_product_attribute">
					Combinação
				</label>
				<div class="col-lg-9 ">
					<select required="required" class="" id="id_product_attribute" name="id_product_attribute">
						<option value="">Selecione a combinação</option>
						{foreach $product.attributes as $attribute}
							<option value="{$attribute.id_product_attribute}">{$attribute.attribute_designation}</option>
						{/foreach}
					</select>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<button class="btn btn-default pull-right" name="submitAssociate" id="form_submit_btn" value="1" type="submit">
				<i class="process-icon-save"></i> Associar
			</button>
			<a onclick="window.history.back()" class="btn btn-default" href="index.php?controller={$smarty.get.controller}&token={$smarty.get.token}&action=list">
				<i class="process-icon-cancel"></i> Cancelar
			</a>
		</div>
	</div>
</form>