<form method="post" action="index.php?controller={$smarty.get.controller}&token={$smarty.get.token}&id={$id}&action=associate"  class="form-horizontal defaultForm">
	<div id="fieldset_0" class="panel">
		<div class="panel-heading">Associação - Produto </div>
		<div class="form-wrapper">
			<div class="form-group">
				<label class="control-label col-lg-2" for="productFilter">Produto</label>
				<div class="input-group col-lg-5">		
					<input type="hidden" id="id_product" name="id_product_ps" value="" />
					<input type="text" id="productFilter" class="input-xlarge" name="productFilter" value=""/>
					<span class="input-group-addon"><i class="icon-search"></i></span>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-2" for=""></label>
				<div class="col-lg-9" >
					<div class="alert alert-info">
						<ul class="list-unstyled ">
							<li>
								<strong>Nome:</strong> {$product.name}
							</li>
							<li>
								<strong>Código do produto:</strong> {$product.codigoProduto}
							</li>
							<li>
								<strong>Código Abacos:</strong> {$product.codigoProdutoAbacos}
							</li>
							<li>
								<strong>EAN:</strong> {$product.ean}
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<button class="btn btn-default pull-right" name="submitAssociate" id="form_submit_btn" value="1" type="submit">
				<i class="process-icon-save"></i> Associar
			</button>
			<a onclick="window.history.back()" class="btn btn-default" href="index.php?controller={$smarty.get.controller}&token={$smarty.get.token}&action=list">
				<i class="process-icon-cancel"></i> Cancelar
			</a>
		</div>
	</div>
</form>
<script type="text/javascript">

	$('#productFilter')
		.autocomplete(
				'{$urlAjaxGetProduct}', {
				minChars: 2,
				width: 500,
				selectFirst: false,
				scroll: false,
				dataType: 'json',
				formatItem: function(data, i, max, value, term) {
					return value;
				},
				parse: function(data) {
					var mytab = new Array();
					for (var i = 0; i < data.length; i++)
						mytab[mytab.length] = { data: data[i], value: data[i].name };
					return mytab;
				},
				extraParams: {
					//controller: '',
					token: "{$smarty.get.token}",
					productFilter: 1
				}
			}
		)
		.result(function(event, data, formatted) {
			console.log(data);
			document.getElementById("id_product").value = data.id_product;
			document.getElementById("productFilter").value = data.name;
		});
</script>